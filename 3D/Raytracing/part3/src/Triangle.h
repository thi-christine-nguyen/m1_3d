#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "Vec3.h"
#include "Ray.h"
#include "Plane.h"
#include <vector>
#include <string>

struct RayTriangleIntersection{
    bool intersectionExists;
    float t;
    float w0,w1,w2;
    unsigned int tIndex;
    Vec3 intersection;
    Vec3 normal;
};

class Triangle {
private:
    Vec3 m_c[3] , m_normal;
    float area;
public:
    Triangle() {}
    Triangle( Vec3 const & c0 , Vec3 const & c1 , Vec3 const & c2 ) {
        m_c[0] = c0;
        m_c[1] = c1;
        m_c[2] = c2;
        updateAreaAndNormal();
    }
    void updateAreaAndNormal() {
        Vec3 nNotNormalized = Vec3::cross( m_c[1] - m_c[0] , m_c[2] - m_c[0] );
        float norm = nNotNormalized.length();
        m_normal = nNotNormalized / norm;
        area = norm / 2.0f;
    }
    void setC0( Vec3 const & c0 ) { m_c[0] = c0; } // remember to update the area and normal afterwards!
    void setC1( Vec3 const & c1 ) { m_c[1] = c1; } // remember to update the area and normal afterwards!
    void setC2( Vec3 const & c2 ) { m_c[2] = c2; } // remember to update the area and normal afterwards!
    Vec3 const & normal() const { return m_normal; }

    Vec3 const & getm0() const { return m_c[0]; }
    Vec3 const & getm1() const { return m_c[1]; }
    Vec3 const & getm2() const { return m_c[2]; }


    Vec3 projectOnSupportPlane( Vec3 const & p ) const {
        Vec3 result;
        //TODO completer
        return result;
    }
    float squareDistanceToSupportPlane( Vec3 const & p ) const {
        float result;
        //TODO completer
        return result;
    }
    float distanceToSupportPlane( Vec3 const & p ) const { return sqrt( squareDistanceToSupportPlane(p) ); }
    bool isParallelTo( Line const & L ) const {
        bool result;
        //TODO completer
        return result;
    }
    Vec3 getIntersectionPointWithSupportPlane( Line const & L ) const {
        // you should check first that the line is not parallel to the plane!
        Vec3 result;
        //TODO completer
        return result;
    }
    void computeBarycentricCoordinates( Vec3 const & p , float & u0 , float & u1 , float & u2 ) const {
        //TODO Complete
    }

       // RayTriangleIntersection getIntersection( Ray const & ray ) const {
    //     RayTriangleIntersection result;
    //     // 1) check that the ray is not parallel to the triangle:

    //     // 2) check that the triangle is "in front of" the ray:

    //     Vec3 d = ray.direction(); 
    //     Vec3 o = ray.origin(); 

    //     Vec3 a = m_c[0]; 
    //     Vec3 b = m_c[1]; 
    //     Vec3 c = m_c[2]; 

    //     Vec3 ab = b - a;
    //     Vec3 ac = c - a;

    //     float D = Vec3::dot(m_normal, a); // distance du plan (on prend un point et la normale)
    //     float t = (D - Vec3::dot(m_normal, o)) / Vec3::dot(m_normal, d); 
      
    //     // si t infini alors rayon parallèle et distinct du plan 
    //     // si t non défini alors rayon confondu avec le plan 
    //     // si t < 0 alors intersection derrière la caméra
    //     // si t > 0 alors intersection devant la caméra
    //     // si t == 0 alors intersection est pile sur la caméra

    //     if(t > 0){

    //         Vec3 intersectionPoint = o + t * d;
    //         Vec3 edge0 = b - a;
    //         Vec3 edge1 = c - a;
    //         Vec3 edge2 = intersectionPoint - a;

    //         float dot00 = Vec3::dot(ab, ab);
    //         float dot01 = Vec3::dot(ab, ac);
    //         float dot02 = Vec3::dot(ab, edge2);
    //         float dot11 = Vec3::dot(ac, ac);
    //         float dot12 = Vec3::dot(ac, edge2);

    //         float invDenom = 1.0f / (dot00 * dot11 - dot01 * dot01);
    //         float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
    //         float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

    //         if (u >= 0.0f && v >= 0.0f && (u + v) <= 1.0f) {
    //             // Intersection point is inside the triangle.
    //             result.intersectionExists = true;
    //             result.t = t;
    //             result.w0 = 1.0f - u - v;
    //             result.w1 = u;
    //             result.w2 = v;
    //             result.intersection = intersectionPoint;
    //             result.normal = m_normal;
    //         } else {
    //             result.intersectionExists = false;
    //             return result;
    //         }

    
    //     }
    //     else {
    //         result.intersectionExists = false;
    //         return result;
    //     }

    //     return result;
    // }
    

  
    RayTriangleIntersection getIntersection( Ray const & ray ) const {
        RayTriangleIntersection result;
        // 1) check that the ray is not parallel to the triangle:

        // 2) check that the triangle is "in front of" the ray:

        // 3) check that the intersection point is inside the triangle:
        // CONVENTION: compute u,v such that p = w0*c0 + w1*c1 + w2*c2, check that 0 <= w0,w1,w2 <= 1

        // 4) Finally, if all conditions were met, then there is an intersection! :
        float epsilon = 1e-6;

        Vec3 d = ray.direction(); 
        Vec3 o = ray.origin(); 

        Vec3 a = m_c[0]; 
        Vec3 b = m_c[1]; 
        Vec3 c = m_c[2]; 

        if (Vec3::dot(m_normal, d) != 0) {
            float D = Vec3::dot(m_normal, a);
            float t = (D - Vec3::dot(m_normal, o))/Vec3::dot(m_normal, d);

            if (t >= 0) {
                Vec3 intersectionPoint = o + t * d;
                
                Vec3 airePar = Vec3::cross(b - a, c - a);
                float aire = Vec3::dot(airePar, m_normal); 

                Vec3 Aa = Vec3::cross(b - a, intersectionPoint - a);
                Vec3 Ab = Vec3::cross(c - b, intersectionPoint - b);
                Vec3 Ac = Vec3::cross(a - c, intersectionPoint - c);
              

                if (Vec3::dot(m_normal, Aa) > 0 && Vec3::dot(m_normal, Ab) > 0 && Vec3::dot(m_normal,Ac) > 0) {

                    float aireAa = Vec3::dot(Aa, m_normal); 
                    float aireAb = Vec3::dot(Ab, m_normal); 
                    float aireAc = Vec3::dot(Ac, m_normal); 
                    

                    float alpha = aireAa/aire;
                    float beta = aireAb/aire;
                    float gamma= aireAc/aire;

                    if (alpha >= -epsilon && beta >= -epsilon && gamma >= -epsilon) {

                        result.intersectionExists = true;
                        result.t = t;
                        result.w0 = alpha;
                        result.w1 = beta;
                        result.w2 = gamma;
                        result.intersection = intersectionPoint;
                        result.normal = m_normal;
                        result.normal.normalize(); 
                    }
                }

                else {
                    result.intersectionExists = false;
                }
            }
            else {
                result.intersectionExists = false;
            }
        }
        else {
            result.intersectionExists = false;
        }

        return result;
    }

};
#endif