#ifndef SQUARE_H
#define SQUARE_H
#include "Vec3.h"
#include <vector>
#include "Mesh.h"
#include <cmath>

struct RaySquareIntersection{
    bool intersectionExists;
    float t;
    float u,v;
    Vec3 intersection;
    Vec3 normal;
};


class Square : public Mesh {
public:
    Vec3 m_normal;
    Vec3 m_bottom_left;
    Vec3 m_right_vector;
    Vec3 m_up_vector;

    Square() : Mesh() {}
    Square(Vec3 const & bottomLeft , Vec3 const & rightVector , Vec3 const & upVector , float width=1. , float height=1. ,
           float uMin = 0.f , float uMax = 1.f , float vMin = 0.f , float vMax = 1.f) : Mesh() {
        setQuad(bottomLeft, rightVector, upVector, width, height, uMin, uMax, vMin, vMax);
    }

    void setQuad( Vec3 const & bottomLeft , Vec3 const & rightVector , Vec3 const & upVector , float width=1. , float height=1. ,
                  float uMin = 0.f , float uMax = 1.f , float vMin = 0.f , float vMax = 1.f) {
        m_right_vector = rightVector;
        m_up_vector = upVector;
        m_normal = Vec3::cross(rightVector , upVector);
        m_bottom_left = bottomLeft;

        m_normal.normalize();
        m_right_vector.normalize();
        m_up_vector.normalize();

        m_right_vector = m_right_vector*width;
        m_up_vector = m_up_vector*height;

        vertices.clear();
        vertices.resize(4);
        vertices[0].position = bottomLeft;                                      vertices[0].u = uMin; vertices[0].v = vMin;
        vertices[1].position = bottomLeft + m_right_vector;                     vertices[1].u = uMax; vertices[1].v = vMin;
        vertices[2].position = bottomLeft + m_right_vector + m_up_vector;       vertices[2].u = uMax; vertices[2].v = vMax;
        vertices[3].position = bottomLeft + m_up_vector;                        vertices[3].u = uMin; vertices[3].v = vMax;
        vertices[0].normal = vertices[1].normal = vertices[2].normal = vertices[3].normal = m_normal;
        triangles.clear();
        triangles.resize(2);
        triangles[0][0] = 0;
        triangles[0][1] = 1;
        triangles[0][2] = 2;
        triangles[1][0] = 0;
        triangles[1][1] = 2;
        triangles[1][2] = 3;


    }

  

    RaySquareIntersection intersect(const Ray &ray) const {
        RaySquareIntersection intersection;

        Vec3 d = ray.direction(); 
        Vec3 o = ray.origin(); 

        Vec3 bottomLeft = vertices[0].position; 
        Vec3 bottomRight = vertices[1].position; 
        Vec3 upLeft = vertices[3].position; 

        Vec3 RL = bottomLeft - bottomRight;
        Vec3 UB = bottomLeft - upLeft;
        Vec3 n = Vec3::cross(RL, UB); // normale

        float D = Vec3::dot(bottomLeft, n); // distance du plan (on prend un point et la normale)
        float t = (D - Vec3::dot(o, n)) / Vec3::dot(d, n); 
      
        // si t infini alors rayon parallèle et distinct du plan 
        // si t non défini alors rayon confondu avec le plan 
        // si t < 0 alors intersection derrière la caméra
        // si t > 0 alors intersection devant la caméra
        // si t == 0 alors intersection est pile sur la caméra

        if (t > 0) {
            Vec3 intersectionPoint = o + t * d;
           
            Vec3 BRP = intersectionPoint - bottomRight;
            float u = (Vec3::dot(RL, BRP))/RL.norm(); // distance position horizontale

            
            Vec3 UP = intersectionPoint - upLeft;
            float v = (Vec3::dot(UB, UP))/UB.norm(); // distance position verticale

            // si u et v sont entre 0 et la longueur du carré alors on garde l'intersection

            //si produit scalaire de la norme et du rayon <= 0 alors on affiche
            if ( u < RL.norm() && u > 0 && v < UB.norm() && v > 0 && Vec3::dot(n, ray.direction()) <= 0) {
                intersection.intersectionExists = true;
                intersection.t = t;
                intersection.u = u;
                intersection.v = v;
                intersection.intersection = intersectionPoint; 
                intersection.normal = n;
                intersection.normal.normalize();
            }
            else {
                intersection.intersectionExists = false;
            }

            return intersection;
            
        }

        return intersection;
            

    }

   

};
#endif // SQUARE_H
