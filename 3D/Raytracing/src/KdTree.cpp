#include "KdTree.h"
#include "Mesh.h"



KdTree::KdTree(Mesh &mesh) {
    // Initialisation de la racine 
    left = nullptr;
    right = nullptr;
    depth = 0;

    Vec3 min = mesh.vertices[0].position;
    Vec3 max = mesh.vertices[0].position;

    for(MeshVertex v : mesh.vertices){
        for(int p = 0; p < 3; p++){
            if(v.position[p] < min[p]) 
                min[p] = v.position[p];
            if(v.position[p] > max[p]) 
                max[p] = v.position[p];
        }
    }

    boundingBox = BoundingBox(min, max);
    triangles = mesh.trianglesObject;
    bestAxis = findBestAxis(boundingBox); 
  
    // maxDepth = 1;
    maxDepth = log2(triangles.size());
    generateKdTree();
}




