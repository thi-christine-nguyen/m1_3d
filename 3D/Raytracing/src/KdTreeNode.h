#ifndef KDTREENODE_H
#define KDTREENODE_H


#include "Vec3.h"
#include "Ray.h"
#include "Triangle.h"
#include "BoundingBox.h"
#include <vector>
#include <string>
#include <iostream>

#include <GL/glut.h>

#include <cfloat>

class KdTreeNode {
    public:
        BoundingBox boundingBox;  // Axis-aligned bounding box
        std::vector<Triangle> triangles;  // Indices of triangles in this node
        KdTreeNode* left;  // Pointer to the left child
        KdTreeNode* right;  // Pointer to the right child
        float constant; 
        int orientation; 
        float planeConstant; 

        KdTreeNode() : left(nullptr), right(nullptr) {}
        KdTreeNode(const BoundingBox& box, const std::vector<Triangle>& tris)
            : boundingBox(box), triangles(tris), left(nullptr), right(nullptr) {}
    };

#endif