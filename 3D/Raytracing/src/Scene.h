#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <string>
#include "Mesh.h"
#include "Sphere.h"
#include "Square.h"
#include "Camera.h"
#include <algorithm>
#include <iostream>

#include <GL/glut.h>


enum LightType {
    LightType_Spherical,
    LightType_Quad
};


struct Light {
    Vec3 material;
    bool isInCamSpace;
    LightType type;

    Vec3 pos;
    float radius;

    Mesh quad;

    float powerCorrection;

    Light() : powerCorrection(1.0) {}

};

struct RaySceneIntersection{
    bool intersectionExists;
    unsigned int typeOfIntersectedObject;
    unsigned int objectIndex;
    float t;
    RayTriangleIntersection rayMeshIntersection;
    RaySphereIntersection raySphereIntersection;
    RaySquareIntersection raySquareIntersection;
    RaySceneIntersection() : intersectionExists(false) , t(FLT_MAX) {}



};



class Scene {
    std::vector< Mesh > meshes;
    std::vector< Sphere > spheres;
    std::vector< Square > squares;
    std::vector< Light > lights;

    // Active le depth of view
    bool depthActive = false; 

    Camera camera; 

    // Focal Length – determines how far objects must be from the camera to be in focus. 
    // Items closer than the focal point will be blurry, as will items farther away.

    float focalLength = 6.f;

        //Aperture – determines how blurry objects that are out of focus will appear. 
        // It defines the range of area around the focal point where objects appear mostly focused. 
        //In the limit case with an aperture size equal to zero, we are back to a pinhole camera where everything in the image is in focus.
        float aperture = 0.5f; 
    

  

public:


    Scene() {
    }

    void setCamera(Camera c){
        camera = c; 
        camera.setDepthOfField(focalLength, aperture);

    }


    void draw() {
       
        for( unsigned int It = 0 ; It < meshes.size() ; ++It ) {
            Mesh const & mesh = meshes[It];
            mesh.draw();
        }
        for( unsigned int It = 0 ; It < spheres.size() ; ++It ) {
            Sphere const & sphere = spheres[It];
            sphere.draw();
        }
        for( unsigned int It = 0 ; It < squares.size() ; ++It ) {
            Square const & square = squares[It];
            square.draw();
        }
    }



    RaySceneIntersection computeIntersection(Ray const & ray) {

        //TODO calculer les intersections avec les objets de la scene et garder la plus proche
        RaySceneIntersection result;
        RaySphereIntersection rayonSphere; 
        RaySquareIntersection rayonSquare;
        RayTriangleIntersection rayonMesh;

       
        
        //Parcourir les objets de la scène ici les sphères
        for (unsigned int i = 0; i < spheres.size(); i++) {

            rayonSphere = spheres[i].intersect(ray);
            // On vérifie que le rayon a une intersection.
            // Si t < 0, alors l'origine du rayon est à l'intérieur de la sphère.
            // Si t >= 0, alors le rayon intersecte la sphère.
            // On compare les intersections pour obtenir l'objet le plus proche.

            if(rayonSphere.intersectionExists && rayonSphere.t >= 0 && rayonSphere.t < result.t ){

                result.raySphereIntersection = rayonSphere;
                result.intersectionExists = true;
                result.objectIndex = i;
                result.typeOfIntersectedObject = 1;
                result.t = rayonSphere.t;
            }
        }

        //Parcourir les objets de la scène ici les squares
        for (unsigned int i = 0; i < squares.size(); i++) {

            rayonSquare = squares[i].intersect(ray);
            // rajout de la condition rayonSquare.t pour ne pas prendre en compte le front wall de la boite de Cornell. 
            if (rayonSquare.intersectionExists && rayonSquare.t >= 0 && rayonSquare.t < result.t ) { 
                result.raySquareIntersection = rayonSquare;
                result.intersectionExists = true;
                result.typeOfIntersectedObject = 2;
                result.objectIndex = i;
                result.t = rayonSquare.t;
            }
        }

        for (unsigned int i = 0; i < meshes.size(); i++) {
            rayonMesh = meshes[i].intersect(ray);

            if(rayonMesh.t >= 0 && rayonMesh.t < result.t && rayonMesh.intersectionExists){
                result.rayMeshIntersection = rayonMesh;
                result.intersectionExists = true;
                result.objectIndex = i;
                result.typeOfIntersectedObject = 3;
                result.t = rayonMesh.t;
            }
        }
       
        
        return result;
    }

    // Retourne la couleur selons les objets et si pas d'objet alors met du blanc
    Vec3 getColorOfObject(RaySceneIntersection intersect) {
        if (intersect.typeOfIntersectedObject == 1) {
            return spheres[intersect.objectIndex].material.diffuse_material;
        }
        else if (intersect.typeOfIntersectedObject == 2) {      
            return squares[intersect.objectIndex].material.diffuse_material;
        }
        else if (intersect.typeOfIntersectedObject == 3) {      
            return meshes[intersect.objectIndex].material.diffuse_material;
        }
        return Vec3(1, 1, 1);
    }

    Vec3 getIntersect(RaySceneIntersection intersect) {
        if (intersect.typeOfIntersectedObject == 1) {
            return intersect.raySphereIntersection.intersection; 
        }
        else if (intersect.typeOfIntersectedObject == 2) {      
            return intersect.raySquareIntersection.intersection; 
        }else if (intersect.typeOfIntersectedObject == 3){
            return intersect.rayMeshIntersection.intersection; 
        }
        return Vec3(1, 1, 1);
    }

    Vec3 getNormal(RaySceneIntersection intersect) {
        if (intersect.typeOfIntersectedObject == 1) {
            return intersect.raySphereIntersection.normal; 
        }
        else if (intersect.typeOfIntersectedObject == 2) {      
            return intersect.raySquareIntersection.normal; 
        }
        else if (intersect.typeOfIntersectedObject == 3) {      
            return intersect.rayMeshIntersection.normal; 
        }
        return Vec3(1, 1, 1);
    }

    Material getMaterial(RaySceneIntersection intersect) {
        if (intersect.typeOfIntersectedObject == 1) {
            return spheres[intersect.objectIndex].material; 
        }
        else if (intersect.typeOfIntersectedObject == 2) {      
            return squares[intersect.objectIndex].material; 
        }
           
        return meshes[intersect.objectIndex].material; 
        
        
    }

    MaterialType getMaterialType(RaySceneIntersection intersect) {
        if (intersect.typeOfIntersectedObject == 1) {
            return spheres[intersect.objectIndex].material.type; 
        }
        else if (intersect.typeOfIntersectedObject == 2) {      
            return squares[intersect.objectIndex].material.type; 
        }
            
        return meshes[intersect.objectIndex].material.type; 
    
        
    }


    Vec3 phong(RaySceneIntersection intersect, Ray ray) {
        
        Vec3 color = Vec3(0, 0, 0);

        Vec3 P = getIntersect(intersect); // Point de la surface
     
        Vec3 N = getNormal(intersect); // Vecteur normal 
        Vec3 V = ray.origin() - P; // Vecteur vue
        V.normalize(); 

        Material material = getMaterial(intersect); // Récupération des propriétés du matérial
        Vec3 ambiantRef, diffuseRef , specularRef; 

        // Lumière ambiante : Ia = Isa * Ka 
        ambiantRef = Vec3::compProduct(lights[0].material, material.ambient_material);  // Intensité de la lumière ambiante * coeff de réflexion ambiante
        color = ambiantRef; 

       
       
        for (unsigned int i = 0; i < lights.size(); i++){
            

            // Lumière diffuse : Id = Isd * Kd * dot(L, N); 
            Vec3 Isd = Vec3::compProduct(lights[i].material, material.diffuse_material); // Intensité de la lumière diffuse * coeff de réflexion diffuse
            
            Vec3 L = lights[i].pos - P; // Vecteur direction lumière
            L.normalize(); 
            
            float dotLN = Vec3::dot(L, N); 
            diffuseRef =  Isd * std::fmax(0.0, dotLN);
           
           
            // Lumière spéculaire : Is = Iss * Ks * dot(R, V)^n
            Vec3 Iss = Vec3::compProduct(lights[i].material, material.specular_material); // Intensité de la lumière spéculaire * coeff de réflexion spéculaire
            Vec3 R = 2.0 * (Vec3::dot(N, L) * N) - L; // Direction de réflexion de la lumière sur un miroir
            R.normalize(); 
            float spec = pow(std::fmax(0.0,Vec3::dot(R, V)), std::fmax(0.0, material.shininess)); // dot(R, V)^n
            specularRef = Iss * spec;

            color += diffuseRef + specularRef;
            // color += diffuseRef;

        }


        return color;
    }

    Vec3 shadow(RaySceneIntersection intersect, Ray ray) {
        Vec3 color;
        Vec3 P = getIntersect(intersect);  // Point de la surface

        Vec3 L = lights[0].pos - P; // Vecteur direction lumière
        float dist = L.length(); // Distance entre le point et la source de lumiière
        L.normalize();

        // Ajout valeur d'écart pour éviter les intersections du rayon avec l'objet à partir duquel il est lancé
        float bias = 0.001f; 
        P += bias * L;

        Ray rayS = Ray(P, L); 
        RaySceneIntersection shadow = computeIntersection(rayS); // Lancer rayon d'ombre

        // Si il y a une intersection avec un objet
        if (shadow.intersectionExists && shadow.t < dist && shadow.typeOfIntersectedObject) {
            color = Vec3(0, 0, 0);
        } else {
            color = phong(intersect, ray);
        }

        return color;
    }




    Vec3 softShadow(RaySceneIntersection intersect, Ray ray) {
        Vec3 color;
        Vec3 P = getIntersect(intersect); // Point de la surface
      

        float numShadowRays = 2; // Nombre de rayons d'ombre qu'on souhaite lancer
        float numNoShadow = 0; // Nombre de fois où il n'y a pas d'intersection avec un objet
        
        //Si on a plusieurs lights 
        for (unsigned int j = 0; j < lights.size(); j++){

            if (lights[j].type == LightType_Quad ){

                // Position des points délimitant le carré à échantilloner
                Vec3 bottomLeft = lights[j].quad.vertices[0].position; 
                Vec3 bottomRight = lights[j].quad.vertices[1].position;
                Vec3 upLeft = lights[j].quad.vertices[3].position;

                for (unsigned int i = 0; i < numShadowRays; i++) {
                    
                    // On choisi un point aléatoire dans le carré (échantillonage)
                    double randomX = double(rand()) / RAND_MAX;
                    double randomZ = double(rand()) / RAND_MAX;
                    Vec3 randomLight = (1.0 - randomX - randomZ) * bottomLeft + randomX * bottomRight + randomZ * upLeft;

                    
                    Vec3 L_shadow = randomLight- P; // Vecteur direction lumière
                    float distShadow = L_shadow.length(); // Distance entre le point choisi au hasard et la source de lumière
                    L_shadow.normalize(); 

                    
                    float bias = 0.001f; 
                    P += bias * L_shadow;


                    Ray rayS = Ray(P, L_shadow);
                    RaySceneIntersection shadow_ray = computeIntersection(rayS); // Lancer un rayon d'ombre

                    // Si il y a pas d'intersection avec un objet
                    if (!(shadow_ray.intersectionExists && shadow_ray.t < distShadow)) {
                        numNoShadow++;
                    }
                }

            }
           
        }

        float softness = numNoShadow / (numShadowRays * lights.size()); // Calcul du pourcentage de rayon sans intersection
        color = phong(intersect, ray) * softness;

        return color;
    }

   
    

    Vec3 rayTraceRecursive( Ray ray , int NRemainingBounces ) {

        RaySceneIntersection raySceneIntersection = computeIntersection(ray);
        Vec3 color;
        if (!raySceneIntersection.intersectionExists) {
            return Vec3(0, 0, 0);
        }

        Material m = getMaterial(raySceneIntersection);

        if(getMaterialType(raySceneIntersection) == Material_Mirror && NRemainingBounces > 0) {

            Vec3 reflection = ray.direction() - 2*(Vec3::dot(ray.direction(), getNormal(raySceneIntersection))) * getNormal(raySceneIntersection);
            
            float bias =  0.001f; 
            Vec3 reflectionRayOrigin = getIntersect(raySceneIntersection) + bias * reflection;
            Ray reflectionRay = Ray(reflectionRayOrigin,  reflection);

        
            NRemainingBounces--; 
            color =  rayTraceRecursive(reflectionRay, NRemainingBounces);
            

        }else if (getMaterialType(raySceneIntersection) == Material_Glass && NRemainingBounces > 0){
            // Loi de Snell pour calculer le vecteur de réfraction
            // ηi * cos(θi) = ηt * cos(θt) 

            Vec3 T = ray.direction();
            Vec3 N = getNormal(raySceneIntersection);

            float cosTheta = Vec3::dot(T, N); 

            float ni = 1.0f; //air
            float nt = m.index_medium;
            Vec3 normal;
            if (cosTheta < 0 ) {
                // Entre dans la glass
                normal =  N; 
                cosTheta = -cosTheta; 
                
              
            } else {
                // Sort de la glass
                ni = m.index_medium; 
                nt = 1.0f; 
                normal = (-1)* N;
                
            }

            // Calcul du vecteur de réfraction
            float eta = ni / nt;
            float k = 1.0f - (eta * eta) * (1.0f - (cosTheta * cosTheta));
           

            if( k > 0){
                Vec3 refraction = (eta * T) + (((eta * cosTheta) - sqrtf(k)) * normal);
               
                float bias =  0.01f; 
                Vec3 refractionRayOrigin = getIntersect(raySceneIntersection) + bias * refraction;
                
                Ray refractionRay = Ray(refractionRayOrigin, refraction);
              
                NRemainingBounces--; 
                color = rayTraceRecursive(refractionRay, NRemainingBounces);
            
            }

          
        }
        else{
            color = phong(raySceneIntersection, ray);
            // color = shadow(raySceneIntersection, ray);
            // color = softShadow(raySceneIntersection, ray); 
        }
        

      

        return color;
    }

    Vec3 rayTrace( Ray const & rayStart ) {
        //TODO appeler la fonction recursive
        Vec3 color;

       
        RaySceneIntersection intersect = computeIntersection(rayStart);
        if (!intersect.intersectionExists) {
            return Vec3(1.,1.,1.);
        } 

        // Depth of view
        if (camera.aperture > 0.0f && depthActive) {

            // Calcul du point de convergence
            Vec3 convergencePoint = rayStart.origin() + camera.focalLength * rayStart.direction();

            // Genere un point dans le disque autour de l'aperture
            float randTheta = ((float)rand() / RAND_MAX) * 2.0f * M_PI;
            float randR = ((float)rand() / RAND_MAX) * camera.aperture;
            Vec3 randomPointOnDisk = randR * Vec3(cos(randTheta), sin(randTheta), 0.0f);

            // Changement de l'origin du rayon avec le nouveau point
            Ray shiftedRay = Ray(rayStart.origin() + randomPointOnDisk, rayStart.direction());

            // Calcule du nouveau rayon C - (O + r)
            Vec3 shiftedDirection = convergencePoint - shiftedRay.origin();
            shiftedDirection.normalize();

            Ray finalShift = Ray(rayStart.origin() + randomPointOnDisk, shiftedDirection); 
            // shiftedRay.setDirection(shiftedDirection);

            // Faire plusieurs sample pour améliorer la qualité
            int numSamples = 10; 
            Vec3 colorSum = Vec3(0, 0, 0);

            for (int i = 0; i < numSamples; ++i) {
                colorSum += rayTraceRecursive(finalShift, 5);
            }

            color = colorSum / numSamples;
        }else{

            color = rayTraceRecursive(rayStart, 5);
        }
            
        return color;
    
    }

    void setup_single_sphere() {
        meshes.clear();
        spheres.clear();
        squares.clear();
        lights.clear();

        {
            lights.resize( lights.size() + 1 );
            Light & light = lights[lights.size() - 1];
            light.pos = Vec3(-5,5,5);
            light.radius = 2.5f;
            light.powerCorrection = 2.f;
            light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;
        }
        {
            spheres.resize( spheres.size() + 1 );
            Sphere & s = spheres[spheres.size() - 1];
            s.m_center = Vec3(1.0, 0.0, -2);
            s.m_radius = 1.f;
            s.build_arrays();
            s.material.type = Material_Mirror;
            s.material.diffuse_material = Vec3( 1.,0.,0 );
            s.material.specular_material = Vec3( 0.2,0.2,0.2 );
            s.material.shininess = 20;
        }

        //Sphère verte
        {
            spheres.resize( spheres.size() + 1 );
            Sphere & s = spheres[spheres.size() - 1];
            s.m_center = Vec3(-1. , 0. , -1.);
            s.m_radius = 2.f;
            s.build_arrays();
            s.material.type = Material_Mirror;
            s.material.diffuse_material = Vec3( 0.,1.,0. );
            s.material.specular_material = Vec3( 0.2,0.2,0.2 );
            s.material.shininess = 20;
        }
    }

    void setup_single_square() {
        meshes.clear();
        spheres.clear();
        squares.clear();
        lights.clear();

        {
            lights.resize( lights.size() + 1 );
            Light & light = lights[lights.size() - 1];
            light.pos = Vec3(-5,5,5);
            light.radius = 2.5f;
            light.powerCorrection = 2.f;
            light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;
        }

        {
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 1.,0.,0 );
            s.material.specular_material = Vec3( 0.8,0.8,0.8 );
            s.material.shininess = 20;
        }
    }

   
    void setup_cornell_box(){
        meshes.clear();
        spheres.clear();
        squares.clear();
        lights.clear();

        {
            lights.resize( lights.size() + 1 );
            Light & light = lights[lights.size() - 1];
            light.pos = Vec3(0.0, 1.5, 0.0);
            // light.pos = Vec3(1.0, -1.25, 0.5);
            light.radius = 2.5f;
            light.quad = Square(Vec3(light.pos[0] - light.radius / 4, 
                                    light.pos[1]- light.radius / 4, 
                                    light.pos[2]- light.radius / 4), 
                                Vec3(1, 0, 0), 
                                Vec3(0, 0, 1), 
                                light.radius, 
                                light.radius);
        
            light.powerCorrection = 0.5f;
            light.type = LightType_Quad;
            // light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;
        }


        { //Back Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.scale(Vec3(2., 2., 1.));
            s.translate(Vec3(0., 0., -2.));
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.5,0.5,1. );
            s.material.specular_material = Vec3( 1.,1.,1. );
            s.material.shininess = 16;
        }

        { //Left Wall

            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.scale(Vec3(2., 2., 1.));
            s.translate(Vec3(0., 0., -2.));
            s.rotate_y(90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 1.,0.,0. );
            s.material.specular_material = Vec3( 1.,0.,0. );
            s.material.shininess = 16;
        }

        { //Right Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_y(-90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.0,1.0,0.0 );
            s.material.specular_material = Vec3( 0.0,1.0,0.0 );
            s.material.shininess = 16;
        }

        { //Floor
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_x(-90);
            s.build_arrays();
            // s.material.diffuse_material = Vec3( 1.0,1.0,1.0 );
             s.material.diffuse_material = Vec3( 1,0.7,0.7 );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
        }

        { //Ceiling
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_x(90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 1.0,1.0,1.0 );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
        }

        { //Front Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_y(180);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 1.0,1.0,0.0 );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
        }


        { //GLASS Sphere

            spheres.resize( spheres.size() + 1 );
            Sphere & s = spheres[spheres.size() - 1];
            s.m_center = Vec3(1.0, -1.25, 0.5);
            s.m_radius = 0.75f;
            s.build_arrays();
            s.material.type = Material_Glass;
            s.material.diffuse_material = Vec3( 1.,0.,0. );
            s.material.specular_material = Vec3( 1.,0.,0. );
            s.material.shininess = 16;
            s.material.transparency = 1.0;
            s.material.index_medium = 1.4;
        }


        { //MIRRORED Sphere
            spheres.resize( spheres.size() + 1 );
            Sphere & s = spheres[spheres.size() - 1];
            s.m_center = Vec3(-1.0, -1.25, -0.5);
            s.m_radius = 0.75f;
            s.build_arrays();
            s.material.type = Material_Mirror;
            s.material.diffuse_material = Vec3( 1.,1.,1. );
            s.material.specular_material = Vec3(  1.,1.,1. );
            s.material.shininess = 16;
            s.material.transparency = 1.0;
            s.material.index_medium = 1.4;
        }

       


    }

    
     
    void suzanne(){

        meshes.clear();
        spheres.clear();
        squares.clear();
        lights.clear();

        {
            lights.resize( lights.size() + 1 );
            Light & light = lights[lights.size() - 1];
            light.pos = Vec3( 0.0, 10, 0);
            light.radius = 2.5f;
            light.quad = Square(Vec3(light.pos[0] - light.radius / 2, 
                                    light.pos[1]- light.radius / 2, 
                                    light.pos[2]- light.radius / 2), 
                                Vec3(1, 0, 0), 
                                Vec3(0, 0, 1), 
                                light.radius, 
                                light.radius);
        
            light.powerCorrection = 2.f;
            light.type = LightType_Quad;
            // light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;
        }



        { 
            meshes.resize(meshes.size() + 1);
            Mesh & m = meshes[meshes.size() - 1];
            m.loadOFF("img/mesh/suzanne.off");
            // m.centerAndScaleToUnit();
            // m.translate(Vec3(0, -1, 0)); 
            m.build_arrays(); 
            m.initializeKdTree(); 
            m.material.diffuse_material = Vec3(1, 0, 1);
            m.material.specular_material = Vec3(1, 0, 1);
            m.material.shininess = 16;
        }

        { //Back Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.scale(Vec3(2., 2., 1.));
            s.translate(Vec3(0., 0., -2.));
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.5, 1, 0. );
            s.material.specular_material = Vec3( 1.,1.,1. );
            s.material.shininess = 16;
        }


        { //Floor
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_x(-90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0, 0.5, 1. );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
        }
        
        //  { //MIRRORED Sphere

        //     spheres.resize( spheres.size() + 1 );
        //     Sphere & s = spheres[spheres.size() - 1];
        //     s.m_center = Vec3(0, 0, 0);
        //     s.m_radius = 0.75f;
        //     s.build_arrays();
        //     s.material.type = Material_Glass;
        //     s.material.diffuse_material = Vec3( 1.,0.,0. );
        //     s.material.specular_material = Vec3( 1.,0.,0. );
        //     s.material.shininess = 16;
        //     s.material.transparency = 1.0;
        //     s.material.index_medium = 2;
        // }
          
    }

    void suzanneBox(){
        meshes.clear();
        spheres.clear();
        squares.clear();
        lights.clear();

        {
            lights.resize( lights.size() + 1 );
            Light & light = lights[lights.size() - 1];
            light.pos = Vec3( 0.0, 1.5, 0.0 );
           
            light.radius = 1.0f;
            light.quad = Square(Vec3(light.pos[0] - light.radius / 4, 
                                    light.pos[1]- light.radius / 4, 
                                    light.pos[2]- light.radius / 4), 
                                Vec3(1, 0, 0), 
                                Vec3(0, 0, 1), 
                                light.radius, 
                                light.radius);
        
            light.powerCorrection = 1.f;
            light.type = LightType_Quad;
            // light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;
        }

        { //Back Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.scale(Vec3(2., 2., 1.));
            s.translate(Vec3(0., 0., -2.));
            s.build_arrays();
            s.material.diffuse_material = Vec3(0, 0, 0.5);
            s.material.specular_material = Vec3( 1.,1.,1. );
            s.material.shininess = 16;
        }

        { //Left Wall

            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.scale(Vec3(2., 2., 1.));
            s.translate(Vec3(0., 0., -2.));
            s.rotate_y(90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.2,0.2,0.3 );
            s.material.specular_material = Vec3( 1.,0.,0. );
            s.material.shininess = 16;
        }

        { //Right Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_y(-90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 1,0.7,0.7 );
            s.material.specular_material = Vec3( 0.0,1.0,0.0 );
            s.material.shininess = 16;
        }

        { //Floor
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_x(-90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 1.,1.,1. );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
        }

        { //Ceiling
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_x(90);
            s.build_arrays();
           s.material.diffuse_material = Vec3( 1.,1.,1. );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
        }

        { //Front Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_y(180);
            s.build_arrays();
           s.material.diffuse_material = Vec3( 1.,1.,1. );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
        }


          { 
            meshes.resize(meshes.size() + 1);
            Mesh & m = meshes[meshes.size() - 1];
            m.loadOFF("img/mesh/suzanne.off");
            m.centerAndScaleToUnit();
            m.translate(Vec3(1, -1, -0.75)); 
            m.build_arrays(); 
            m.initializeKdTree(); 
            m.material.type = Material_Glass; 
            m.material.diffuse_material = Vec3(1, 0, 0.5);
            m.material.specular_material = Vec3(1, 0, 1);
            m.material.shininess = 16;
        }


        //   { 
        //     meshes.resize(meshes.size() + 1);
        //     Mesh & m = meshes[meshes.size() - 1];
        //     m.loadOFF("img/mesh/suzanne.off");
        //     m.centerAndScaleToUnit();
        //     m.translate(Vec3(-1, -1, 1)); 
        //     m.build_arrays(); 
        //     m.initializeKdTree(); 
        //     // m.material.type = Material_Mirror; 
        //     m.material.diffuse_material = Vec3(1, 0.5, 1);
        //     m.material.specular_material = Vec3(1, 0, 1);
        //     m.material.shininess = 16;
        // }

        //   { 
        //     meshes.resize(meshes.size() + 1);
        //     Mesh & m = meshes[meshes.size() - 1];
        //     m.loadOFF("img/mesh/suzanne.off");
        //     m.centerAndScaleToUnit();
        //     m.translate(Vec3(0, 1, -1)); 
        //     m.build_arrays(); 
        //     m.initializeKdTree(); 
        //     // m.material.type = Material_Mirror; 
        //     m.material.diffuse_material = Vec3(1, 0, 1);
        //     m.material.specular_material = Vec3(1, 0, 1);
        //     m.material.shininess = 16;
        // }
    }

   


};



#endif