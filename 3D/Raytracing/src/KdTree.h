#ifndef KDTREE_H
#define KDTREE_H

#include "BoundingBox.h"
#include <cfloat>

class Mesh;

class KdTree {
private:
    int maxDepth;
    int bestAxis; 
    BoundingBox boundingBox; 
    std::vector<Triangle> triangles;  
    KdTree* left;  
    KdTree* right; 
    int depth;  

   
public:
   

    KdTree() : left(nullptr), right(nullptr) {}
    KdTree(Mesh &mesh);
    KdTree(BoundingBox box,  std::vector<Triangle> tri, int d){
        boundingBox = box; 
        triangles = tri; 
        depth = d; 
        left = nullptr; 
        right = nullptr; 

    }

    void generateKdTree() {
        // Création de l'arbre

        if (triangles.size() < 5 || depth >= maxDepth) {
            return;
        }

        int bestAxis = findBestAxis(boundingBox);

        // Tri des triangles selon l'axe
        std::vector<Triangle> triangleTrie = triTriangle(triangles, bestAxis);
      

        int mid = triangles.size() / 2; 

        // Tri les triangles en left et right de meme taille
        std::vector<Triangle> leftTriangle(triangleTrie.begin(), triangleTrie.begin() + mid);
        std::vector<Triangle> rightTriangle(triangleTrie.begin() + mid, triangleTrie.end());

        // Création des noeuds enfant
        left = new KdTree();
        createNode(*left, leftTriangle, bestAxis); 

        right  = new KdTree();
        createNode(*right, rightTriangle, bestAxis); 

    }

    BoundingBox getBounding();

    void setBoundingBox(BoundingBox b){
        boundingBox = b; 
    }
    void setDepth(int d){
        depth = d; 
    }
    void setMaxDepth(int max){
        maxDepth = max; 
    }
    void setBestAxis(int axis){
        bestAxis = axis; 
    }
 

   

    bool rayIntersectsBoundingBox(const Ray& ray, const BoundingBox& box) {
        // Verifie si le rayon intersection la bounding box

        float tMin = (box.min[0] - ray.origin()[0]) / ray.direction()[0];
        float tMax = (box.max[0] - ray.origin()[0]) / ray.direction()[0];

        if (tMin > tMax) std::swap(tMin, tMax);

        float tyMin = (box.min[1] - ray.origin()[1]) / ray.direction()[1];
        float tyMax = (box.max[1] - ray.origin()[1]) / ray.direction()[1];

        if (tyMin > tyMax) std::swap(tyMin, tyMax);

        if ((tMin > tyMax) || (tyMin > tMax)) {
            return false;
        }

        if (tyMin > tMin) tMin = tyMin;
        if (tyMax < tMax) tMax = tyMax;

        float tzMin = (box.min[2] - ray.origin()[2]) / ray.direction()[2];
        float tzMax = (box.max[2] - ray.origin()[2]) / ray.direction()[2];

        if (tzMin > tzMax) std::swap(tzMin, tzMax);

        if ((tMin > tzMax) || (tzMin > tMax)) {
            return false;
        }

        return true;
    }

    int findBestAxis(BoundingBox boundingBox){
        // Donne l'axis le plus long

        float x = boundingBox.max[0] - boundingBox.min[0]; 
        float y = boundingBox.max[1] - boundingBox.min[1]; 
        float z = boundingBox.max[2] - boundingBox.min[2]; 

        if (x <= y && y <= z){
            return 2; 
        }else if(x <= y && z <= y){
            return 1; 
        }
        return 0; 
    }


    std::vector<Triangle>  triTriangle(std::vector<Triangle> triangles, int axe) {
       
        auto compareTriangles = [axe](const Triangle& a, const Triangle& b) {
            return a.getCentroid()[axe] < b.getCentroid()[axe];
        };

        // Tri des triangles selon l'axe
        std::sort(triangles.begin(), triangles.end(), compareTriangles);
      
        return triangles;
    }


    RayTriangleIntersection intersectNode(const Ray& ray) {
        RayTriangleIntersection result;
        result.t = FLT_MAX;  // Initialize t to a large value.

        // Si le rayon intersecte la bounding box 
        if (rayIntersectsBoundingBox(ray, boundingBox)) {   
            
            // Si c'est une feuille chercher l'intersection la plus proche parmis les triangles de la feuille
            if (right == nullptr && left==nullptr) {
                
                for(unsigned int i = 0; i < triangles.size(); i++){
                    
                    RayTriangleIntersection intersection = triangles[i].getIntersection(ray);
                  
                    if (intersection.intersectionExists && intersection.t < result.t) {
                        result = intersection;  
                        result.t = intersection.t; 
                        result.intersectionExists = true; 
                        result.tIndex = triangles[i].index;
                        result.normal = intersection.normal;
                        result.normal.normalize(); 
                        
                    }
                }

                return result; 
            } else {
                // Si c'est pas une feuille, récursion sur les noeuds enfant

                RayTriangleIntersection leftResult = left->intersectNode(ray);
                RayTriangleIntersection rightResult = right->intersectNode(ray);

               
                if (leftResult.intersectionExists && leftResult.t < result.t) {
                    result = leftResult;
                }
                if (rightResult.intersectionExists && rightResult.t < result.t) {
                    result = rightResult;
                }
                
            }
        }

        return result;
    }

    
    void draw(Vec3 color) const{
       
        boundingBox.draw(color);
        if (left != NULL) {
            left->draw(color);
            
        }
        if (right != NULL) {
            right->draw(color);
        }
    }

    void createNode(KdTree &node, std::vector<Triangle> tri, int axis) {
             
        node.setBoundingBox(BoundingBox(tri));
        node.triangles = tri; 
        node.setDepth(depth + 1);
        node.setMaxDepth(maxDepth);
        node.setBestAxis(axis);
        node.generateKdTree();
    }


};

#endif
