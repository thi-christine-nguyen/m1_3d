#ifndef KDTREE_H
#define KDTREE_H

#include "BoundingBox.h"
#include <cfloat>

class Mesh;

class KdTree {
private:
    int maxDepth;
    int bestAxis; 
    BoundingBox boundingBox;  // Axis-aligned bounding box
    std::vector<Triangle> triangles;  // Indices of triangles in this node
    KdTree* left;  // Pointer to the left child
    KdTree* right;  // Pointer to the right child
    Mesh* m; 

    int depth;  

   
public:
   

    KdTree() : left(nullptr), right(nullptr) {}
    KdTree(Mesh &mesh);
    KdTree(BoundingBox box,  std::vector<Triangle> tri, int d){
        boundingBox = box; 
        triangles = tri; 
        depth = d; 
        left = nullptr; 
        right = nullptr; 

    }

    void generateKdTree() {
        if (triangles.size() < 5 || depth >= maxDepth) {
            return;
        }

        int bestAxis = findBestAxis(boundingBox);
        // std::cout << triangles.size() << std::endl; 
        std::vector<Triangle> triangleTrie = triTriangle(triangles, bestAxis);
      

        int mid = triangles.size() / 2; 

        std::vector<Triangle> leftTriangle(triangleTrie.begin(), triangleTrie.begin() + mid);
        std::vector<Triangle> rightTriangle(triangleTrie.begin() + mid, triangleTrie.end());

      
        // for(Triangle t : leftTriangle){
        //     std::cout<< depth << " depth " << t.getCentroid()[bestAxis] << std::endl; 
        // }

        left = new KdTree();
        createNode(*left, leftTriangle, bestAxis); 

        // std::cout << left->depth << " - nbrTriangleLeft : " << left->triangles.size() << std::endl;
        // std::cout << right->depth << " - right : " << right->triangles.size() << std::endl;

        right  = new KdTree();
        createNode(*right, rightTriangle, bestAxis); 

        // std::cout << "   '->  left : " << left->triangles.size() << " / right : " << right->triangles.size() << std::endl;
        

    }

    BoundingBox getBounding();

    void setBoundingBox(BoundingBox b){
        boundingBox = b; 
    }
    void setDepth(int d){
        depth = d; 
    }
    void setMaxDepth(int max){
        maxDepth = max; 
    }
    void setBestAxis(int axis){
        bestAxis = axis; 
    }
 

   

    bool rayIntersectsBoundingBox(const Ray& ray, const BoundingBox& box) {
        float tMin = (box.min[0] - ray.origin()[0]) / ray.direction()[0];
        float tMax = (box.max[0] - ray.origin()[0]) / ray.direction()[0];

        if (tMin > tMax) std::swap(tMin, tMax);

        float tyMin = (box.min[1] - ray.origin()[1]) / ray.direction()[1];
        float tyMax = (box.max[1] - ray.origin()[1]) / ray.direction()[1];

        if (tyMin > tyMax) std::swap(tyMin, tyMax);

        if ((tMin > tyMax) || (tyMin > tMax)) {
            return false;
        }

        if (tyMin > tMin) tMin = tyMin;
        if (tyMax < tMax) tMax = tyMax;

        float tzMin = (box.min[2] - ray.origin()[2]) / ray.direction()[2];
        float tzMax = (box.max[2] - ray.origin()[2]) / ray.direction()[2];

        if (tzMin > tzMax) std::swap(tzMin, tzMax);

        if ((tMin > tzMax) || (tzMin > tMax)) {
            return false;
        }

        return true;
    }

    int findBestAxis(BoundingBox boundingBox){
        // std::cout << "hehe" << std::endl; 
        // std::cout << boundingBox.min << std::endl; 
        // std::cout << boundingBox.max << std::endl; 

        float x = boundingBox.max[0] - boundingBox.min[0]; 
        float y = boundingBox.max[1] - boundingBox.min[1]; 
        float z = boundingBox.max[2] - boundingBox.min[2]; 

        // std::cout << " x" << x << " y" << y << " z" << z << std::endl; 

        if (x <= y && y <= z){
            return 2; 
        }else if(x <= y && z <= y){
            return 1; 
        }
        return 0; 
    }


    std::vector<Triangle>  triTriangle(std::vector<Triangle> triangles, int axe) {
        // Fonction de comparaison personnalisée
        auto compareTriangles = [axe](const Triangle& a, const Triangle& b) {
            return a.getCentroid()[axe] < b.getCentroid()[axe];
        };

        // Tri des triangles le long de l'axe spécifié
        std::sort(triangles.begin(), triangles.end(), compareTriangles);
      
        return triangles;
    }

    std::vector<Triangle> interTriangle(const Ray& ray){
        std::vector<Triangle> interTri;
        
        if (!rayIntersectsBoundingBox(ray, boundingBox)) {
           return interTri; 
                
        }else{
            if (left != nullptr){
                std::vector<Triangle> leftTri = left->interTriangle(ray);
                interTri.insert(interTri.end(), leftTri.begin(), leftTri.end());
            }
            if (right != nullptr){
                std::vector<Triangle>rightTri = right->interTriangle(ray);
                interTri.insert(interTri.end(),rightTri.begin(),rightTri.end());
            }
            if (left == nullptr && right == nullptr && !triangles.empty()) {
                return this->triangles;
            }
            
        }
        // std::cout <<interTri.size() << std::endl; 
        return interTri; 

    }

    // int depth = 0; 

    // RayTriangleIntersection intersectNode(const Ray& ray) {
    //     RayTriangleIntersection result;
    //     result.t = FLT_MAX;  // Initialize t to a large value.
       
    //     // std::cout << node->myString << std::endl; 

    //     if (rayIntersectsBoundingBox(ray, boundingBox)) {
            
    //         // if (triangles.size() < 5 || depth >= maxDepth) {
    //         if (right == nullptr && left==nullptr) {
                
    //             for(int i = 0; i < triangles.size(); i++){
                    
    //                 // std::cout << i << std::endl; 
    //                 RayTriangleIntersection intersection = triangles[i].getIntersection(ray);
                  
    //                 if (intersection.intersectionExists && intersection.t < result.t) {
    //                     result = intersection;  
    //                     result.t = intersection.t; 
    //                     result.intersectionExists = true; 
    //                     result.tIndex = i;

    //                     result.normal = intersection.normal;
    //                     result.normal.normalize(); 
                        
                       
    //                 }
    //             }

    //             return result; 
    //         } else {


    //             RayTriangleIntersection leftResult = left->intersectNode(ray);
    //             RayTriangleIntersection rightResult = right->intersectNode(ray);

               
    //             if (leftResult.intersectionExists && leftResult.t < result.t) {
    //                 result = leftResult;
    //             }
    //             if (rightResult.intersectionExists && rightResult.t < result.t) {
    //                 result = rightResult;
    //             }
                
    //         }
    //     }

    //     return result;
    // }


    // RayTriangleIntersection intersectionKdTree(const Ray& ray) {
        
    //     return intersectNode(ray);
    // }
    
    void draw(Vec3 color) const{
       
        boundingBox.draw(color);
      

        if (left != NULL) {
            // std::cout << left->triangles.size() << std::endl; 
            left->draw(color);
            
        }
        if (right != NULL) {
            right->draw(color);
        }
    }

    void createNode(KdTree &node, std::vector<Triangle> tri, int axis) {
             
        node.setBoundingBox(BoundingBox(tri));
        // std::cout << "axis = " << axis << std::endl; 
        // std::cout << node.boundingBox.max << std::endl; 
        // std::cout << tri[tri.size()-1].getm0() << std::endl; 
        // std::cout << tri[tri.size()-1].getm1()<< std::endl; 
        // std::cout << tri[tri.size()-1].getm2() << std::endl; 

        node.triangles = tri; 
        node.setDepth(depth + 1);
        node.setMaxDepth(maxDepth);
        node.setBestAxis(axis);
        node.generateKdTree();
    }


};

#endif
