#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "Vec3.h"
#include "Ray.h"
#include "Plane.h"
#include <vector>
#include <string>

struct RayTriangleIntersection{
    bool intersectionExists;
    float t;
    float w0,w1,w2;
    unsigned int tIndex;
    Vec3 intersection;
    Vec3 normal;
};

class Triangle {
private:
    Vec3 m_c[3] , m_normal;
    float area;
   
public:
    Vec3 boundMin; 
    Vec3 boundMax;
    int index; 
    Triangle() {}
    Triangle( Vec3 const & c0 , Vec3 const & c1 , Vec3 const & c2 ) {
        m_c[0] = c0;
        m_c[1] = c1;
        m_c[2] = c2;
        
        updateAreaAndNormal();
    }
    void updateAreaAndNormal() {
        Vec3 nNotNormalized = Vec3::cross( m_c[1] - m_c[0] , m_c[2] - m_c[0] );
        float norm = nNotNormalized.length();
        m_normal = nNotNormalized / norm;
        area = norm / 2.0f;
    }
    void setC0( Vec3 const & c0 ) { m_c[0] = c0; } // remember to update the area and normal afterwards!
    void setC1( Vec3 const & c1 ) { m_c[1] = c1; } // remember to update the area and normal afterwards!
    void setC2( Vec3 const & c2 ) { m_c[2] = c2; } // remember to update the area and normal afterwards!
    Vec3 const & normal() const { return m_normal; }

    Vec3 const & getm0() const { return m_c[0]; }
    Vec3 const & getm1() const { return m_c[1]; }
    Vec3 const & getm2() const { return m_c[2]; }

    std::vector<Vec3> getBound(){

        std::vector<Vec3> bound; 

        bound.push_back(Vec3(
            std::min(std::min(m_c[0][0], m_c[1][0]), m_c[2][0]),
            std::min(std::min(m_c[0][1], m_c[1][1]), m_c[2][1]),
            std::min(std::min(m_c[0][2], m_c[1][2]), m_c[2][2])
        ));

        bound.push_back(Vec3(
            std::max(std::max(m_c[0][0], m_c[1][0]), m_c[2][0]),
            std::max(std::max(m_c[0][1], m_c[1][1]), m_c[2][1]),
            std::max(std::max(m_c[0][2], m_c[1][2]), m_c[2][2])
        ));


        // bound.push_back(Vec3(xMin, yMin, zMin)); 
        // bound.push_back(Vec3(xMax, yMax, zMax)); 

        return bound; 
        
    }

    Vec3 const & getPoint(int i) const {return m_c[i]; }

    Vec3 getCentroid() const {
        return (getm0() + getm1() + getm2()) / 3.0f;
    }



    Vec3 projectOnSupportPlane( Vec3 const & p ) const {
        Vec3 result;
        //TODO completer
        return result;
    }
    float squareDistanceToSupportPlane( Vec3 const & p ) const {
        float result;
        //TODO completer
        return result;
    }
    float distanceToSupportPlane( Vec3 const & p ) const { return sqrt( squareDistanceToSupportPlane(p) ); }
    bool isParallelTo( Line const & L ) const {
        bool result;
        //TODO completer
        return result;
    }
    Vec3 getIntersectionPointWithSupportPlane( Line const & L ) const {
        // you should check first that the line is not parallel to the plane!
        Vec3 result;
        //TODO completer
        return result;
    }
    void computeBarycentricCoordinates( Vec3 const & p , float & u0 , float & u1 , float & u2 ) const {
        //TODO Complete
    }

  
    RayTriangleIntersection getIntersection( Ray const & ray ) const {
        RayTriangleIntersection result;
        // 1) check that the ray is not parallel to the triangle:

        // 2) check that the triangle is "in front of" the ray:

        // 3) check that the intersection point is inside the triangle:
        // CONVENTION: compute u,v such that p = w0*c0 + w1*c1 + w2*c2, check that 0 <= w0,w1,w2 <= 1

        // 4) Finally, if all conditions were met, then there is an intersection! :
        float epsilon = 1e-6;

        Vec3 d = ray.direction(); 
        Vec3 o = ray.origin(); 

        Vec3 a = m_c[0]; 
        Vec3 b = m_c[1]; 
        Vec3 c = m_c[2]; 

        if (Vec3::dot(m_normal, d) != 0) {
            float D = Vec3::dot(m_normal, a);
            float t = (D - Vec3::dot(m_normal, o))/Vec3::dot(m_normal, d);

            if (t >= 0) {
                Vec3 intersectionPoint = o + t * d;
                
                Vec3 airePar = Vec3::cross(b - a, c - a);
                float aire = Vec3::dot(airePar, m_normal); 

                Vec3 Aa = Vec3::cross(b - a, intersectionPoint - a);
                Vec3 Ab = Vec3::cross(c - b, intersectionPoint - b);
                Vec3 Ac = Vec3::cross(a - c, intersectionPoint - c);
              

                if (Vec3::dot(m_normal, Aa) > 0 && Vec3::dot(m_normal, Ab) > 0 && Vec3::dot(m_normal,Ac) > 0) {

                    float aireAa = Vec3::dot(Aa, m_normal); 
                    float aireAb = Vec3::dot(Ab, m_normal); 
                    float aireAc = Vec3::dot(Ac, m_normal); 
                    

                    float alpha = aireAa/aire;
                    float beta = aireAb/aire;
                    float gamma= aireAc/aire;

                    if (alpha >= 0 && beta >= 0 && gamma >= 0) {

                        result.intersectionExists = true;
                        result.t = t;
                        result.w0 = alpha;
                        result.w1 = beta;
                        result.w2 = gamma;
                        result.intersection = intersectionPoint;
                        result.normal = m_normal;
                        result.normal.normalize(); 
                    }
                }

                else {
                    result.intersectionExists = false;
                }
            }
            else {
                result.intersectionExists = false;
            }
        }
        else {
            result.intersectionExists = false;
        }

        return result;
    }

};
#endif