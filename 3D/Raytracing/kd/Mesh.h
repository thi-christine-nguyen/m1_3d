#ifndef MESH_H
#define MESH_H


#include <vector>
#include <string>
#include "Vec3.h"
#include "Ray.h"
#include "Triangle.h"
#include "Material.h"
#include "KdTree.h"

#include <GL/glut.h>

#include <cfloat>


// -------------------------------------------
// Basic Mesh class
// -------------------------------------------

struct MeshVertex {
    inline MeshVertex () {}
    inline MeshVertex (const Vec3 & _p, const Vec3 & _n) : position (_p), normal (_n) , u(0) , v(0) {}
    inline MeshVertex (const MeshVertex & vertex) : position (vertex.position), normal (vertex.normal) , u(vertex.u) , v(vertex.v) {}
    inline virtual ~MeshVertex () {}
    inline MeshVertex & operator = (const MeshVertex & vertex) {
        position = vertex.position;
        normal = vertex.normal;
        u = vertex.u;
        v = vertex.v;
        return (*this);
    }
    // membres :
    Vec3 position; // une position
    Vec3 normal; // une normale
    float u,v; // coordonnees uv
    
};

struct MeshTriangle {
    inline MeshTriangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline MeshTriangle (const MeshTriangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline MeshTriangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    unsigned int & operator [] (unsigned int iv) { return v[iv]; }
    unsigned int operator [] (unsigned int iv) const { return v[iv]; }
    inline virtual ~MeshTriangle () {}
    inline MeshTriangle & operator = (const MeshTriangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    // membres :
    unsigned int v[3];
};




class Mesh {
protected:
    void build_positions_array() {
        positions_array.resize( 3 * vertices.size() );
        for( unsigned int v = 0 ; v < vertices.size() ; ++v ) {
            positions_array[3*v + 0] = vertices[v].position[0];
            positions_array[3*v + 1] = vertices[v].position[1];
            positions_array[3*v + 2] = vertices[v].position[2];
        }
    }
    void build_normals_array() {
    
        normalsArray.resize( 3 * vertices.size() );
        for( unsigned int v = 0 ; v < vertices.size() ; ++v ) {
            normalsArray[3*v + 0] = vertices[v].normal[0];
            normalsArray[3*v + 1] = vertices[v].normal[1];
            normalsArray[3*v + 2] = vertices[v].normal[2];
        }
    }
    void build_UVs_array() {
        uvs_array.resize( 2 * vertices.size() );
        for( unsigned int vert = 0 ; vert < vertices.size() ; ++vert ) {
            uvs_array[2*vert + 0] = vertices[vert].u;
            uvs_array[2*vert + 1] = vertices[vert].v;
        }
    }
    void build_triangles_array() {
    
       

        triangles_array.resize( 3 * triangles.size() );
        for( unsigned int t = 0 ; t < triangles.size() ; ++t ) {
            triangles_array[3*t + 0] = triangles[t].v[0];
            triangles_array[3*t + 1] = triangles[t].v[1];
            triangles_array[3*t + 2] = triangles[t].v[2];

            unsigned int v0 = triangles[t].v[0];
            unsigned int v1 = triangles[t].v[1];
            unsigned int v2 = triangles[t].v[2];

            Triangle triangle =  Triangle(vertices[v0].position, vertices[v1].position, vertices[v2].position);
            triangle.index = t; 
            trianglesObject.push_back(triangle); 

            std::vector<Vec3> triangleNormal; 
            triangleNormal.push_back(vertices[v0].normal); 
            triangleNormal.push_back(vertices[v1].normal); 
            triangleNormal.push_back(vertices[v2].normal); 

            trianglesObjectNormal.push_back(triangleNormal); 

        }

       
       
      
      
    }


public:
    std::vector<MeshVertex> vertices;
    std::vector<MeshTriangle> triangles;
    std::vector<Triangle> trianglesObject; 
    std::vector<std::vector<Vec3> > trianglesObjectNormal; 

    std::vector< float > positions_array;
    std::vector< float > normalsArray;
    std::vector< float > uvs_array;
    std::vector< unsigned int > triangles_array;

    Material material;
    Vec3 boundMin; 
    Vec3 boundMax;

    KdTree kdTree;
    bool isKdTreeInitialized = false; 
   
    
    void loadOFF (const std::string & filename);
    void recomputeNormals ();
    void centerAndScaleToUnit ();
    void scaleUnit ();


    virtual
    void build_arrays() {
        recomputeNormals();
        build_positions_array();
        build_normals_array();
        build_UVs_array();
        build_triangles_array();
        // initializeKdTree(); 

        // if (!isKdTreeInitialized) {
        //     initializeKdTree();  // Appel de l'initialisation du KdTree
        //     isKdTreeInitialized = true;
        // }

        
        float xMin;  
        float yMin;   
        float zMin;   

        float xMax;   
        float yMax;   
        float zMax; 

        for( unsigned int v = 0 ; v < vertices.size() ; ++v ) {
            if(xMin > vertices[v].position[0]){
                xMin = vertices[v].position[0]; 
            }
            if (yMin > vertices[v].position[1]){
                yMin = vertices[v].position[1]; 
            }
            if (zMin > vertices[v].position[2]){
                zMin = vertices[v].position[2]; 
            }

            if (xMax < vertices[v].position[0]){
                xMax = vertices[v].position[0]; 
            }
            if (yMax < vertices[v].position[1]){
                yMax = vertices[v].position[1]; 
            }
            if (zMax < vertices[v].position[2]){
                zMax = vertices[v].position[2]; 
            }
        }

    

        boundMin = Vec3(xMin, yMin, zMin); 
        boundMax = Vec3(xMax, yMax, zMax); 

     


    }


    void translate( Vec3 const & translation ){
        for( unsigned int v = 0 ; v < vertices.size() ; ++v ) {
            vertices[v].position += translation;
        }

        
        float xMin;  
        float yMin;   
        float zMin;   

        float xMax;   
        float yMax;   
        float zMax; 

        for( unsigned int v = 0 ; v < vertices.size() ; ++v ) {
            if(xMin > vertices[v].position[0]){
                xMin = vertices[v].position[0]; 
            }
            if (yMin > vertices[v].position[1]){
                yMin = vertices[v].position[1]; 
            }
            if (zMin > vertices[v].position[2]){
                zMin = vertices[v].position[2]; 
            }

            if (xMax < vertices[v].position[0]){
                xMax = vertices[v].position[0]; 
            }
            if (yMax < vertices[v].position[1]){
                yMax = vertices[v].position[1]; 
            }
            if (zMax < vertices[v].position[2]){
                zMax = vertices[v].position[2]; 
            }
        }

    

        boundMin = Vec3(xMin, yMin, zMin); 
        boundMax = Vec3(xMax, yMax, zMax); 

    }

    
    void initializeKdTree(){
        // std::cout <<"hehe" << std::endl; 
        kdTree = KdTree(*this); 
      
    }


    void apply_transformation_matrix( Mat3 transform ){
        for( unsigned int v = 0 ; v < vertices.size() ; ++v ) {
            vertices[v].position = transform*vertices[v].position;
        }

        //        recomputeNormals();
        //        build_positions_array();
        //        build_normals_array();
    }

    void scale( Vec3 const & scale ){
        Mat3 scale_matrix(scale[0], 0., 0.,
                0., scale[1], 0.,
                0., 0., scale[2]); //Matrice de transformation de mise à l'échelle
        apply_transformation_matrix( scale_matrix );
    }

    void rotate_x ( float angle ){
        float x_angle = angle * M_PI / 180.;
        Mat3 x_rotation(1., 0., 0.,
                        0., cos(x_angle), -sin(x_angle),
                        0., sin(x_angle), cos(x_angle));
        apply_transformation_matrix( x_rotation );
    }

    void rotate_y ( float angle ){
        float y_angle = angle * M_PI / 180.;
        Mat3 y_rotation(cos(y_angle), 0., sin(y_angle),
                        0., 1., 0.,
                        -sin(y_angle), 0., cos(y_angle));
        apply_transformation_matrix( y_rotation );
    }

    void rotate_z ( float angle ){
        float z_angle = angle * M_PI / 180.;
        Mat3 z_rotation(cos(z_angle), -sin(z_angle), 0.,
                        sin(z_angle), cos(z_angle), 0.,
                        0., 0., 1.);
        apply_transformation_matrix( z_rotation );
    }


    void draw() const {
        kdTree.draw(Vec3(1, 1, 1));
      
        if( triangles_array.size() == 0 ) return;
        GLfloat material_color[4] = {material.diffuse_material[0],
                                     material.diffuse_material[1],
                                     material.diffuse_material[2],
                                     1.0};

        GLfloat material_specular[4] = {material.specular_material[0],
                                        material.specular_material[1],
                                        material.specular_material[2],
                                        1.0};

        GLfloat material_ambient[4] = {material.ambient_material[0],
                                       material.ambient_material[1],
                                       material.ambient_material[2],
                                       1.0};
         
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material_specular);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material_color);
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material_ambient);
        glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, material.shininess);

        glEnableClientState(GL_VERTEX_ARRAY) ;
        glEnableClientState (GL_NORMAL_ARRAY);
        glNormalPointer (GL_FLOAT, 3*sizeof (float), (GLvoid*)(normalsArray.data()));
        glVertexPointer (3, GL_FLOAT, 3*sizeof (float) , (GLvoid*)(positions_array.data()));
        glDrawElements(GL_TRIANGLES, triangles_array.size(), GL_UNSIGNED_INT, (GLvoid*)(triangles_array.data()));
       
    }

    

    RayTriangleIntersection intersect( Ray const & ray ) {
        RayTriangleIntersection closestIntersection;
        closestIntersection.t = FLT_MAX;
        
        //Intersection du rayon avec le kdTree
        std::vector<Triangle> tri = kdTree.interTriangle(ray); 

       

        // return kdTree.intersectionKdTree(ray); 

        for(Triangle t : tri){
        
            float indext0 = triangles[t.index][0]; 
            float indext1 = triangles[t.index][1]; 
            float indext2 = triangles[t.index][2]; 

            Vec3 n0 = vertices[indext0].normal; 
            // n0.normalize(); 
            Vec3 n1 = vertices[indext1].normal; 
            // n1.normalize(); 
            Vec3 n2 = vertices[indext2].normal; 
            // n2.normalize(); 

            RayTriangleIntersection intersectionTriangle = t.getIntersection(ray);

            if (intersectionTriangle.intersectionExists && intersectionTriangle.t < closestIntersection.t) {
            
                Vec3 normalInterpo = n2 * intersectionTriangle.w0 + n0 * intersectionTriangle.w1 + n1 * intersectionTriangle.w2; 
                closestIntersection = intersectionTriangle; 
                closestIntersection.tIndex = t.index;
                closestIntersection.normal = normalInterpo;
                closestIntersection.normal.normalize(); 

            }

          
        }
        return closestIntersection;




        // Note :
        // Creer un objet Triangle pour chaque face
        // Vous constaterez des problemes de précision
        // solution : ajouter un facteur d'échelle lors de la création du Triangle : float triangleScaling = 1.000001;
        // float triangleScaling = 1.000001;
        // float epsilon = 0.001f;
      
        // for( unsigned int i = 0 ; i < triangles.size(); i++) {

        //     float indext0 = triangles[i][0]; 
        //     float indext1 = triangles[i][1]; 
        //     float indext2 = triangles[i][2]; 

        //     Vec3 n0 = vertices[indext0].normal; 
        //     // n0.normalize(); 
        //     Vec3 n1 = vertices[indext1].normal; 
        //     // n1.normalize(); 
        //     Vec3 n2 = vertices[indext2].normal; 
        //     // n2.normalize(); 

        //     Vec3 p0 = vertices[indext0].position; 
        //     Vec3 p1 = vertices[indext1].position; 
        //     Vec3 p2 = vertices[indext2].position; 

        //     Triangle triangle =  Triangle(p1, p2, p0);

        //     RayTriangleIntersection intersectionTriangle = triangle.getIntersection(ray);

        //     if (intersectionTriangle.intersectionExists && intersectionTriangle.t < closestIntersection.t) {
               
        //         Vec3 normalInterpo = n0 * intersectionTriangle.w0 + n1 * intersectionTriangle.w1 + n2 * intersectionTriangle.w2; 
        //         closestIntersection = intersectionTriangle; 
        //         closestIntersection.tIndex = i;
        //         closestIntersection.normal = normalInterpo;
        //         closestIntersection.normal.normalize(); 

        //     }
           
        // }
        // return closestIntersection;


    }

    
    


       
};




#endif
