// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

// Include GLEW
#include <GL/glew.h>

// Include GLM
#define GLM_ENABLE_EXPERIMENTAL
#include <third_party/glm-0.9.7.1/glm/glm.hpp>
#include <third_party/glm-0.9.7.1/glm/gtc/matrix_transform.hpp>
#include <third_party/glm-0.9.7.1/glm/gtc/type_ptr.hpp>
#include <iostream>
#include <GL/glut.h>

using namespace glm;
using Vec3 = glm::vec3;

#include "src/shader.hpp"
#include "src/objloader.hpp"

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
glm::vec3 camera_position   = glm::vec3(0.0f, 0.0f,  3.0f);
glm::vec3 camera_target = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 camera_up    = glm::vec3(0.0f, 1.0f,  0.0f);
glm::vec3 camera_target_lateral = glm::vec3(-1.0f, 0.0f, 0.0f);

// timing
float deltaTime = 0.1f;	// time between current frame and last frame
float lastFrame = 0.0f;
float timeCst = 0.0f;

//rotation
float angle = 0.;
float zoom = 1.;

static GLint window;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;

float translate_x = 0.1;
float translate_y = 0.1;


float translate_x2 = 0.1;
float translate_y2 = 0.1;

float translate_x3 = 0.1;
float translate_y3 = 0.1;

float scale_value = 0.5f; 
float rotation = 0.0f;



GLuint programID;
GLuint VertexArrayID;
GLuint vertexbuffer;
GLuint elementbuffer;
GLuint LightID;


std::vector<unsigned short> indices; //Triangles concaténés dans une liste
std::vector<std::vector<unsigned short> > triangles;
std::vector<glm::vec3> indexed_vertices;


glm::mat4 ViewMatrix;
glm::mat4 ProjectionMatrix;

glm::mat4 getViewMatrix(){
	return ViewMatrix;
}
glm::mat4 getProjectionMatrix(){
	return ProjectionMatrix;
}

glm::mat4 transformation = glm::mat4(1.0f);



// Initial position : on +Z
glm::vec3 position = glm::vec3( 0, 0, 0 );
// Initial horizontal angle : toward -Z
float horizontalAngle = 3.14f;
// Initial vertical angle : none
float verticalAngle = 0.0f;
// Initial Field of View
float initialFoV = 45.0f;

float speed = 3.0f; // 3 units / second
float mouseSpeed = 0.005f;


	// Right vector
glm::vec3 rightVector() {
    return glm::vec3(
		sin(horizontalAngle - 3.14f/2.0f),
		0,
		cos(horizontalAngle - 3.14f/2.0f)
	);
}

// Direction : Spherical coordinates to Cartesian coordinates conversion
glm::vec3 directionVector() {
    return glm::vec3(
        cos(verticalAngle) * sin(horizontalAngle),
        sin(verticalAngle),
        cos(verticalAngle) * cos(horizontalAngle)
    );
}

void computeMatricesFromInputs(float moveX, float moveY);
void initLight ();
void init ();
void draw ();
void display ();
void idle ();
void key (unsigned char keyPressed, int x, int y);
void mouse (int button, int state, int x, int y);
void motion (int x, int y);
void reshape(int w, int h);
int main (int argc, char ** argv);
void printMatrix(const glm::mat4& mat);

// ------------------------------------

void printMatrix(const glm::mat4& mat) {
    std::cout << mat[0][0] << " " << mat[1][0] << " " << mat[2][0] << " " << mat[3][0] << "\n" << mat[0][1] << " " << mat[1][1] << " " << mat[2][1] << " " << mat[3][1] << "\n" << mat[0][2] << " " << mat[1][2] << " " << mat[2][2] << " " << mat[3][2] << "\n" << mat[0][3] << " " << mat[1][3] << " " << mat[2][3] << " " << mat[3][3] << std::endl;
}

void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    // camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    // glCullFace (GL_BACK);
    // glEnable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return;
    }

}




// ------------------------------------
// rendering.
// ------------------------------------
void draw () {
    glUseProgram(programID);

    // Exercice 1 :
    // Première chaise
    // glm::mat4 transformation = glm::mat4(1.0f);
    // GLuint position = glGetUniformLocation(programID, "transformation");
    // transformation = glm::mat4(1.0f);
    // transformation = glm::translate(transformation, glm::vec3(translate_x, translate_y, 0));
    // transformation = glm::scale(transformation, glm::vec3(scale_value, scale_value, scale_value) );
    // glUniformMatrix4fv(position, 1, GL_FALSE, glm::value_ptr(transformation));

    // glEnableVertexAttribArray(0);
    // glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    // glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    // glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);


    // Deuxième chaise
    // glm::mat4 transformation2 = glm::mat4(1.0f);
    // transformation2 = glm::translate(transformation2, glm::vec3(translate_x2, translate_y2, 0));
    // transformation2 = glm::scale(transformation2, glm::vec3(scale_value, scale_value, scale_value));
    // transformation2 = glm::rotate(transformation2, 1.f, glm::vec3(0, 1, 0));
    // transformation2 = glm::rotate(transformation2, 1.f, glm::vec3(0, 1, 0));
    // transformation2 = glm::rotate(transformation2, 1.f, glm::vec3(0, 1, 0));
    // //transformation2 = glm::translate(transformation2, glm::vec3(1.0f, 0.0f, 0.0f));


    // GLuint position2 = glGetUniformLocation(programID, "transformation");
    // glUniformMatrix4fv(position2, 1, GL_FALSE, glm::value_ptr(transformation2));
    // glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    // glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);


    // //Troisième chaise 
    // glm::mat4 transformation3 = glm::mat4(1.0f);
    // transformation3 = glm::rotate(transformation3, rotation, glm::vec3(0.0f, 0.0f, 0.5f));

    // GLuint position3 = glGetUniformLocation(programID, "transformation");
    // glUniformMatrix4fv(position3, 1, GL_FALSE, glm::value_ptr(transformation3));

    // glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    // glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);

    
    //Suzanne
    // glm::mat4 transformation = glm::mat4(1.0f);
    // GLuint position = glGetUniformLocation(programID, "transformation");
    // transformation = glm::mat4(1.0f);
    // transformation = glm::translate(transformation, glm::vec3(translate_x, translate_y, 0));
    // transformation = glm::scale(transformation, glm::vec3(scale_value, scale_value, scale_value) );
    // transformation = glm::rotate(transformation, rotation, glm::vec3(0, 1, 0));
    // glUniformMatrix4fv(position, 1, GL_FALSE, glm::value_ptr(transformation));

    // glEnableVertexAttribArray(0);
    // glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    // glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    // glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);


    // Suzanne alignement repère monde 

    // glm::mat4 transformation = glm::mat4(1.0f);
    // glm::vec3 characterUp = glm::vec3(0, 1, 0);
    // glm::vec3 worldUp = glm::vec3(1, 1, 1);

    // characterUp = glm::normalize(characterUp);
    // worldUp = glm::normalize(worldUp);

    // // Calcul de l'angle
    // float dotProduct = glm::dot(characterUp, worldUp);
    // float angle = glm::acos(dotProduct);

    // // Calcul de l'axe
    // glm::vec3 rotationAxis = glm::cross(characterUp, worldUp);

    // // Création de la matrice de rotation
    // glm::mat4 rotationMatrix = glm::rotate(glm::mat4(1.0f), angle, rotationAxis);

    // transformation = glm::scale(transformation, glm::vec3(scale_value, scale_value, scale_value)); 

    // transformation = rotationMatrix * transformation;

    // GLuint position = glGetUniformLocation(programID, "transformation");
    // glUniformMatrix4fv(position, 1, GL_FALSE, glm::value_ptr(transformation));

    // glEnableVertexAttribArray(0);
    // glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    // glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    // glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);


    // Exercice 3 : 
    // glm::mat4 projection, view, modele; 

    // projection = glm::perspective(glm::radians(45.f), 4.0f/3.0f, 0.1f, 100.0f);
    // view = glm::lookAt(camera_position, camera_target, camera_up);

    // GLuint positionProjection = glGetUniformLocation(programID, "projection");
    // GLuint positionView = glGetUniformLocation(programID, "view");

    // glUniformMatrix4fv(positionProjection, 1 , GL_FALSE, glm::value_ptr(projection));
    // glUniformMatrix4fv(positionView, 1 , GL_FALSE,  glm::value_ptr(view));

    // GLuint position = glGetUniformLocation(programID, "modele");
    // modele = glm::mat4(1.0f);
    // glUniformMatrix4fv(position, 1, GL_FALSE, glm::value_ptr(modele));

    // glEnableVertexAttribArray(0);
    // glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    // glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    // glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);


    //Exercice 4 : 

    glm::mat4 projection, view, modele, terre, soleil, lune; 
    timeCst += deltaTime;

    projection = glm::perspective(glm::radians(45.f), 4.0f/3.0f, 0.1f, 100.0f);
    view = glm::lookAt(camera_position, camera_target, camera_up);
    GLuint positionProjection = glGetUniformLocation(programID, "projection");
    GLuint positionView = glGetUniformLocation(programID, "view");

    glUniformMatrix4fv(positionProjection, 1 , GL_FALSE, glm::value_ptr(projection));
    glUniformMatrix4fv(positionView, 1 , GL_FALSE,  glm::value_ptr(view));

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
   

    //Soleil
    GLuint position = glGetUniformLocation(programID, "modele");
    
    soleil = glm::scale(soleil, glm::vec3(0.5, 0.5, 0.5) );
    glUniformMatrix4fv(position, 1, GL_FALSE, glm::value_ptr(soleil));
    
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);

    // Terre
    terre = glm::scale(terre, glm::vec3(0.2f, 0.2f, 0.2f));

    //Fait tourner la terre autout du soleil
    terre = glm::rotate(terre, timeCst, glm::vec3(0, 1, 0));

    //Fait tourner la terre sur elle même
    terre = glm::translate(terre, glm::vec3(0.0f, 0.0f, 4.0f));
    terre = glm::rotate(terre, 23.44f, glm::vec3(0, 0, 1));
    terre = glm::rotate(terre, timeCst, glm::vec3(0, 1, 0));
    

    GLuint position2 = glGetUniformLocation(programID, "modele");
    glUniformMatrix4fv(position2, 1, GL_FALSE, glm::value_ptr(terre));

    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);

    //Lune 

    lune = scale(lune, glm::vec3(0.09f, 0.09f, 0.09f));
    lune = rotate(lune, 6.68f, glm::vec3(0, 0, 1));
    lune = rotate(lune, timeCst, glm::vec3(0, 1, 0));
    lune = translate(lune, glm::vec3(0, 0, 7.f));

    GLuint position3 = glGetUniformLocation(programID, "modele");
    glUniformMatrix4fv(position3, 1, GL_FALSE, glm::value_ptr(lune));

    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);

    glDisableVertexAttribArray(0);
}


void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
    float time = glutGet(GLUT_ELAPSED_TIME) / 1000.f;
    deltaTime = time - lastFrame;
    lastFrame = time;
}

void key (unsigned char keyPressed, int x, int y) {
    float cameraSpeed = 10 * deltaTime;
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCR_WIDTH, SCR_HEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;

    case 's':
        camera_position -= cameraSpeed * camera_target;
        break;

    case 'w':
        camera_position += cameraSpeed * camera_target;
        break;
    
    case 'a' : 
        camera_position -= cameraSpeed * camera_target_lateral;
        std::cout << "h" << std::endl;
        break;

    case 'e' : 
        camera_position += cameraSpeed * camera_target_lateral;
        break;


    case 'd': //Press d key to translate on x positive
        //Completer : mettre à jour le x du Vec3 translate
        translate_x += 0.1; 
        

        break;

    case 'q': //Press q key to translate on x negative
        //Completer : mettre à jour le y du Vec3 translate
        translate_x -= 0.1; 
        
        
        break;

    case 'z': //Press z key to translate on y positive
        //Completer : mettre à jour le y du Vec3 translate
        translate_y += 0.1; 
         
        break;

    case 'c': //Press s key to translate on y negative
        //Completer : mettre à jour le y du Vec3 translate
        translate_y -= 0.1; 
        break;

    case '+': //Press z key to translate on y positive
        //Completer : mettre à jour le y du Vec3 translate
        scale_value += 0.005; 
       
        break;

    case '-': //Press s key to translate on y negative
        //Completer : mettre à jour le y du Vec3 translate
        scale_value -= 0.005; 
    
    case '1' : 
        // a. Réduire la dimension de la chaise par 2, la poser sur le sol et légèrement à gauche.

        scale_value = 0.5; 
        translate_x = -0.75; 
        translate_y = -1; 
      
        break;

    case '2' : 
        // b. Rajouter une chaise en face
        scale_value = 0.5; 
        translate_x = -0.75; 
        translate_y = -1; 

        translate_x2 = 0.75; 
        translate_y2 = -1; 
      
        break;

    case '3' : 
        // b. Rajouter une chaise en face
        scale_value = 0.5; 
        translate_x = -0.75; 
        translate_y = -1; 

        translate_x2 = 0.75; 
        translate_y2 = -1; 

        translate_x3 = 0; 
        translate_y3 = 0.5; 

        break;

    case '4' : 
        // b. Rajouter une chaise en face
        scale_value = 0.5; 
        translate_x = -0.75; 
        translate_y = -1; 

        translate_x2 = 0.75; 
        translate_y2 = -1; 

        translate_x3 = 0; 
        translate_y3 = 0.5; 

        break;
    
    case 'r' : 
        rotation += 0.1; 
        break; 


    default:
        break;
    }
    //TODO add translations
    idle ();
}

void specialKeys(int key, int x, int y) {
    if(key == GLUT_KEY_LEFT)
		position -= rightVector() * deltaTime * speed;
    else if(key == GLUT_KEY_RIGHT)
		position += rightVector() * deltaTime * speed;
    else if(key == GLUT_KEY_DOWN)
		position -= directionVector() * deltaTime * speed;
    else if(key == GLUT_KEY_UP)
        position += directionVector() * deltaTime * speed;
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            //camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
            lastX = x;
            lastY = y;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        computeMatricesFromInputs(x - lastX, y - lastY);
        lastX = x;
        lastY = y;
    }
    else if (mouseMovePressed == true) {
    }
    else if (mouseZoomPressed == true) {
    }
}

void computeMatricesFromInputs(float moveX, float moveY){
    std::cout << moveX << " " << moveY << std::endl;
	// Compute new orientation
	horizontalAngle += mouseSpeed * moveX / 10.f;
	verticalAngle   += mouseSpeed * moveY / 10.f;

	// Up vector
	glm::vec3 up = glm::cross( rightVector(), directionVector() );

	float FoV = initialFoV;

	// Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	ProjectionMatrix = glm::perspective(glm::radians(FoV), 4.0f / 3.0f, 0.1f, 100.0f);

	// Camera matrix
	ViewMatrix       = glm::lookAt(
								camera_position,           // Camera is here
								camera_position + directionVector(), // and looks here : at the same position, plus "direction"
								up                  // Head is up (set to 0,-1,0 to look upside-down)
						   );
}


void reshape(int w, int h) {
    // camera.resize (w, h);
}

int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCR_WIDTH, SCR_HEIGHT);
    window = glutCreateWindow ("TP HAI719I");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    glutSpecialFunc(specialKeys);
    key ('?', 0, 0);

    computeMatricesFromInputs(0.f, 0.f);

    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // Create and compile our GLSL program from the shaders
    programID = LoadShaders( "vertex_shader.glsl", "fragment_shader.glsl" );

    //Chargement du fichier de maillage
    std::string filename("data/sphere.off");
    loadOFF(filename, indexed_vertices, indices, triangles );

    // Load it into a VBO

    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);

    // Generate a buffer for the indices as well
    glGenBuffers(1, &elementbuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0] , GL_STATIC_DRAW);

    // Get a handle for our "LightPosition" uniform
    glUseProgram(programID);
    LightID = glGetUniformLocation(programID, "LightPosition_worldspace");

    glutMainLoop ();

    // Cleanup VBO and shader
    glDeleteBuffers(1, &vertexbuffer);
    glDeleteBuffers(1, &elementbuffer);
    glDeleteProgram(programID);
    glDeleteVertexArrays(1, &VertexArrayID);


    return EXIT_SUCCESS;
}
