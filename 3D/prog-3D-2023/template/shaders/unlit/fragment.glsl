#version 330 core

in vec3 o_positionWorld;
in vec3 o_normalWorld;
in vec2 o_uv0;
out vec4 FragColor;

uniform vec4 color;
uniform sampler2D colorTexture;
uniform sampler2D normaleTexture; 


uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 viewPos; 
uniform samplerCube skybox;


in vec3 TangentLightPos;
in vec3 TangentViewPos;
in vec3 TangentFragPos;
in mat3 TBN; 

in vec3 N;
in vec3 P;



void main() {
    
    
	float ratio = 1.00 / 1.52;

    vec3 I = normalize(P - viewPos);
    vec3 R = reflect(I, normalize(N));
    FragColor = vec4(texture(skybox, R).rgb, 1.0);

    // vec3 normal = texture(normaleTexture, o_uv0).rgb; 
    // // FragColor = vec4(normal, 1.0); 
    // // return; 
    // normal = normal * 2.0 - 1.0; 
   
   
    // // normal = TBN * normal; 
    // // normal = normalize(normal); 
    // // ambient
    // float ambientStrength = 0.5;
    // vec3 ambient = ambientStrength * lightColor ;
    // // diffuse
    // vec3 norm = normalize ( TBN * normal ) ;
    // vec3 lightDir = normalize ( lightPos - o_positionWorld ) ;
    // float diff = max ( dot ( norm , lightDir ) , 0.0) ;
    // vec3 diffuse = diff * lightColor ;
    // // specular
    // float specularStrength = 0.5;
    // vec3 viewDir = normalize ( viewPos - o_positionWorld ) ;
    // vec3 reflectDir = reflect ( - lightDir , norm ) ;
    // float spec = pow ( max ( dot ( viewDir , reflectDir ) , 0.0) , 32) ;
    // vec3 specular = specularStrength * spec * lightColor ;
    // vec3 result = ( ambient + diffuse + specular ) ;
    // FragColor = vec4 ( result , 1.0) ;






    // vec3 normal = texture(normaleTexture, o_uv0).rgb; 
    // normal = normalize(normal * 2.0 - 1.0); 
    // // ambient
    // float ambientStrength = 0.5;
    // vec3 ambient = ambientStrength * lightColor ;
    // // diffuse
    // vec3 norm = normalize ( normal ) ;
    // vec3 lightDir = normalize ( lightPos - o_positionWorld ) ;
    // float diff = max ( dot ( norm , lightDir ) , 0.0) ;
    // vec3 diffuse = diff * lightColor ;
    // // specular
    // float specularStrength = 0.5;
    // vec3 viewDir = normalize ( viewPos - o_positionWorld ) ;
    // vec3 reflectDir = reflect ( - lightDir , norm ) ;
    // float spec = pow ( max ( dot ( viewDir , reflectDir ) , 0.0) , 32) ;
    // vec3 specular = specularStrength * spec * lightColor ;
    // vec3 result = ( ambient + diffuse + specular ) ;
    // FragColor = vec4 ( result , 1.0) ;

    // texture(normaleTexture, o_uv0).rgb
    // FragColor = color;




    // FragColor *= texture(colorTexture, o_uv0); 




    // FragColor *= texture(colorTexture, o_uv0) * color;
    // FragColor += texture(colorTexture, o_uv0) * color * texture(normaleTexture, o_uv0); 
    // DEBUG: position
    //FragColor = vec4(o_positionWorld, 1.0);
    // DEBUG: normal
    // FragColor = vec4(0.5 * o_normalWorld + vec3(0.5) , 1.0);
    // DEBUG: uv0
    // FragColor = vec4(o_uv0, 1.0);
}
