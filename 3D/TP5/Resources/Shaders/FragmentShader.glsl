#version 450 core // Minimal GL version support expected from the GPU

struct LightSource {
    vec3 position;
    vec3 color;
    float intensity;
    int isActive;
    mat4 depthMVP; 
}; int number_of_light = 3; 


uniform LightSource lightSources[3];


struct Material {
    vec3 albedo;
    float shininess;
};


uniform Material material;


// uniform mat4 depthMVP;  // Matrice de vue-projection de la caméra de la carte des ombres
uniform sampler2D shadowMap;  // Texture de la carte des ombres



in vec3 fPosition; // Shader input, linearly interpolated by default from the previous stage (here the vertex shader)
in vec3 fPositionWorldSpace;
in vec3 fNormal;
in vec2 fTexCoord;


out vec4 colorResponse; // Shader output: the color response attached to this fragment


uniform mat4 projectionMat, modelViewMat, normalMat;

float ShadowCalculation(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r; 
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // add a bias to the depth values
    const float bias = 0.005; // Experiment with this value to find what works best for your scene
    closestDepth -= bias;

    // check whether current frag pos is in shadow
    float shadow = currentDepth > closestDepth ? 1.0 : 0.0;

    return shadow;
}




void main() {
    vec3 n = normalize(fNormal);
    
    // Linear barycentric interpolation does not preserve unit vectors
    vec3 wo = normalize (-fPosition); // unit vector pointing to the camera
    vec3 radiance = vec3(0,0,0);
                        
    // Transform the fragment's position to the clip space of the light
  
    if( dot( n , wo ) >= 0.0 ) {
        {
            for (int i = 0; i < number_of_light; ++i){
                if( lightSources[i].isActive == 1 ) { // WE ONLY CONSIDER LIGHTS THAT ARE SWITCHED ON
                 {
                    vec3 wi = normalize ( vec3((modelViewMat * vec4(lightSources[i].position,1)).xyz) - fPosition ); // unit vector pointing to the light source (change if you use several light sources!!!)
                    if( dot( wi , n ) >= 0.0 ) { // WE ONLY CONSIDER LIGHTS THAT ARE ON THE RIGHT HEMISPHERE (side of the tangent plane)
                        vec4 shadowCoord =  lightSources[i].depthMVP * vec4(fPositionWorldSpace, 1.0);
                        float shadow = 1.0 - ShadowCalculation(shadowCoord);
                        vec3 wh = normalize( wi + wo ); // half vector (if wi changes, wo should change as well)
                        vec3 Li = lightSources[i].color * lightSources[i].intensity * shadow; 
                        
                        radiance = radiance +
                                Li // light color
                                * material.albedo
                                * ( max(dot(n,wi),0.0) + pow(max(dot(n,wh),0.0),material.shininess) )
                                ;
                        
                        // radiance *= shadow; 
                            
                    }
                }
            }

            }
           
        }
    }

    


    colorResponse = vec4 (radiance, 1.0); // Building an RGBA value from an RGB one.
}




