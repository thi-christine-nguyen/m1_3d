// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"
#include "src/FastNoiseLit.h"

int indiceImage(int x, int y, int w) {
    return y*w + x;
}

Vec3 normalize(Vec3 vec){
    float length = vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]; 
    length = std::sqrt(length); 

    return vec/length; 
}


struct Triangle {
    inline Triangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline Triangle (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline Triangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    unsigned int & operator [] (unsigned int iv) { return v[iv]; }
    unsigned int operator [] (unsigned int iv) const { return v[iv]; }
    inline virtual ~Triangle () {}
    inline Triangle & operator = (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    // membres :
    unsigned int v[3];
};


struct Mesh {
    std::vector< Vec3 > vertices;
    std::vector< Vec3 > normals;
    std::vector< Triangle > triangles;
    std::vector< Vec3 > colors;
};

Mesh mesh;

//Meshes to generate
Mesh unit_sphere;
Mesh tesselation;
Mesh unit_cylinder;
Mesh unit_cone;
Mesh unit_cube;
int nX = 5;
int nY = 5;
float noiseX = 1; 
float noiseY = 1; 

bool display_normals;
bool display_loaded_mesh;
bool display_unit_sphere;
bool display_unit_cylinder;
bool display_unit_cone;
bool display_unit_cube;
bool display_tesselation;

// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 1600;
static unsigned int SCREENHEIGHT = 900;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;
static FastNoiseLite noise;

Vec3 crossProduct(Vec3& a, Vec3& b) {
    return Vec3(
        a[1] * b[2] - a[2] * b[1],
        a[2] * b[0] - a[0] * b[2],
        a[0] * b[1] - a[1] * b[0]
    );
}

//To complete for Exercice 1
void setUnitSphere( Mesh & o_mesh, int nX=20, int nY=20 )
{
    //nx = nombre de méridiens
    //ny = nombre de parallèles 

    nX = float(nX);
    nY = float(nY);
    float x, y, z, phi, theta; 
    o_mesh.vertices.clear(); 
    o_mesh.normals.clear(); 
    o_mesh.triangles.clear();

    for(int i = 0; i < nX; i++){
       
        for(int j = 0; j < nY; j++){
            theta = float(i) * 2.f * M_PI/float(nX-1) - M_PI; // angle horizontal entre -pi et pi)
            phi = float(j) * M_PI/(nY-1) - M_PI/2.f; // angle vertical (entre -pi/2 et pi/2)

            //float r = noise.GetNoise((float) theta*7.5 , phi * 7.5); 
            
            x =  cos(theta) * cos(phi);
            y = sin(theta) * cos(phi);
            z = sin(phi);

            o_mesh.vertices.push_back(Vec3(x, y, z));
            o_mesh.normals.push_back(Vec3(x, y, z));
            o_mesh.colors.push_back(Vec3(x, y, z)); 


        }
    }

    //Pour faire les triangles : 
    // - parcourir les points et trouver les voisin p0 (notre point), p1 (bas), p2 (droite) 
    // - ajouter à o_mesh.triangles

    for(int i = 0; i < nX-1 ; i++){
        for (int j = 0; j < nY-1; j++){
            int p0 = indiceImage(i, j, nX); // point courant 
            int p1 = indiceImage(i, j+1, nX); // en dessous du point courant
            int p2 = indiceImage(i+1, j, nX); // à droite de p0
            int p3 = indiceImage(i+1, j+1, nX); // en bas à droite
            o_mesh.triangles.push_back(Triangle(p0, p2, p1)); //le premier point est l'angle droit puis suivre le sens des aiguille d'une montre
            o_mesh.triangles.push_back(Triangle(p3, p1, p2));
        }
    }  

}

void setUnitCylinder(Mesh & o_mesh, int nX) {
    o_mesh.vertices.clear();
    o_mesh.normals.clear();
    o_mesh.triangles.clear();
    nX = float(nX);
  

    for(int i = 0; i < nX; i++) {
        float theta = float(i) * 2.f * M_PI/float(nX-1) - M_PI;

        float x = cos(theta) * cos(0);
        float y = sin(theta) * cos(0);
        float z = 0;

        o_mesh.vertices.push_back(Vec3(x, y, z));
        o_mesh.normals.push_back(Vec3(x, y, 0));
    }

    for(int i = 0; i < nX; i++) {
        float theta = float(i) * 2.f * M_PI/float(nX-1) - M_PI;

        float x = cos(theta) * cos(0);
        float y = sin(theta) * cos(0);
        float z = 1;

        o_mesh.vertices.push_back(Vec3(x, y, z));
        o_mesh.normals.push_back(Vec3(x, y, 0));
    }

    o_mesh.vertices.push_back(Vec3(0, 0, 0));
    o_mesh.normals.push_back(Vec3(0, 0, -1));
    o_mesh.vertices.push_back(Vec3(0, 0, 1));
    o_mesh.normals.push_back(Vec3(0, 0, 1));


    for(int i = 0; i < nX-1; i++) {
        int p0 = i;
        int p1 = i+1;
        int p2 = i+nX;
        int p3 = i+nX+1;
        int pCentre0 = nX+nX;
        int pCentre1 = nX+nX+1;
        // Pour les faces sur les côtés
        o_mesh.triangles.push_back(Triangle(p0, p1, p2));
        o_mesh.triangles.push_back(Triangle(p2, p1, p3));

        // Pour les faces circulaires (haut et bas)
        o_mesh.triangles.push_back(Triangle(p1, p0, pCentre0));
        o_mesh.triangles.push_back(Triangle(p2, p3, pCentre1));
    }
}

void setUnitCone(Mesh & o_mesh, int nX) {
    o_mesh.vertices.clear();
    o_mesh.normals.clear();
    o_mesh.triangles.clear();
    nX = float(nX);
    std::vector<Vec3> tempNormal(nX + 2);
   

    for(int i = 0; i < nX; i++) {
        float theta = float(i) * 2.f * M_PI/float(nX-1) - M_PI;

        float x = cos(theta) * cos(0);
        float y = sin(theta) * cos(0);
        float z = 0;

        o_mesh.vertices.push_back(Vec3(x, y, z));
     
    }

    o_mesh.vertices.push_back(Vec3(0, 0, 0));
    o_mesh.vertices.push_back(Vec3(0, 0, 1));
   

    for(int i = 0; i < nX-1; i++) {
        int p0 = i;
        int p1 = i+1;
        int pCentre0 = nX;
        int pCentre1 = nX+1;
        // Pour les faces
        o_mesh.triangles.push_back(Triangle(p0, p1, pCentre1));
        Vec3 v0 = o_mesh.vertices[p0];
        Vec3 v1 = o_mesh.vertices[p1];
        Vec3 vCentre1 = o_mesh.vertices[pCentre1]; 
        Vec3 v4 = v0 - vCentre1;
        Vec3 v5 = v1 - vCentre1;
        Vec3 t1 = normalize(crossProduct(v4, v5));
        tempNormal[p0] += t1; 
        tempNormal[p1] += t1; 
        tempNormal[pCentre1] += t1; 


        o_mesh.triangles.push_back(Triangle(p1, p0, pCentre0));
    
        Vec3 vCentre0 = o_mesh.vertices[pCentre0]; 
        Vec3 v6 = v0 - vCentre0;
        Vec3 v7 = v1 - vCentre0;
        Vec3 t2 = normalize(crossProduct(v6, v7));
        tempNormal[p0] += t2; 
        tempNormal[p1] += t2; 
        tempNormal[pCentre0] += t2; 

    }

    
    for (int i = 0; i < o_mesh.vertices.size(); i++) {

        if(o_mesh.vertices[i][0] == 0 && o_mesh.vertices[i][1] == 0 && o_mesh.vertices[i][2] == 0){

            o_mesh.normals.push_back(normalize(tempNormal[i]/(nX-1)));

        }else if (o_mesh.vertices[i][0] == 0 && o_mesh.vertices[i][1] == 0 && o_mesh.vertices[i][2] == 1)
        {
            o_mesh.normals.push_back(normalize(tempNormal[i]));
        }else {
            o_mesh.normals.push_back(normalize(tempNormal[i]/4));

        }
        

           
    }


}

void setUnitCube1(Mesh & o_mesh) {
    o_mesh.vertices.clear();
    o_mesh.normals.clear();
    o_mesh.triangles.clear();

    for(int z = 0; z < 2; z++) {
        for(int x = 0; x < 2; x++) {
            for(int y = 0; y < 2; y++) {
                o_mesh.vertices.push_back(Vec3(x, y, z));
                // o_mesh.normals.push_back(Vec3(x, y, z));
            }
        }
    }

   

    o_mesh.triangles.push_back(Triangle(0, 1, 2));
    o_mesh.triangles.push_back(Triangle(2, 1, 3));
    o_mesh.triangles.push_back(Triangle(6, 7, 4));
    o_mesh.triangles.push_back(Triangle(7, 5, 4));
    o_mesh.triangles.push_back(Triangle(4, 5, 0));
    o_mesh.triangles.push_back(Triangle(5, 1, 0));
    o_mesh.triangles.push_back(Triangle(2, 3, 6));
    o_mesh.triangles.push_back(Triangle(6, 3, 7));
    o_mesh.triangles.push_back(Triangle(4, 0, 6));
    o_mesh.triangles.push_back(Triangle(6, 0, 2));
    o_mesh.triangles.push_back(Triangle(1, 5, 3));
    o_mesh.triangles.push_back(Triangle(3, 5, 7));
}

void setUnitCube(Mesh &o_mesh) {
    o_mesh.vertices.clear();
    o_mesh.normals.clear();
    o_mesh.triangles.clear();
    int nX = 1; 
    int nY = 1;

    // Vertices
    for (int z = 0; z <= nY; z++) {
        for (int x = 0; x <= nX; x++) {
            for (int y = 0; y <= nY; y++) {
            
                o_mesh.vertices.push_back(Vec3(x, y, z));       
                o_mesh.normals.push_back(Vec3(x, y, z));     
            }
        }
    }
    // for (int z = 0; z < nY; z++) {
    //     for (int x = 0; x < nX; x++) {
    //         for (int y = 0; y < nY; y++) {
    //             int v0 = (z * (nX + 1) + x) * (nY + 1) + y;
    //             int v1 = v0 + 1;
    //             int v2 = v0 + nY + 1;
    //             int v3 = v2 + 1;

    //             // First triangle of the face
    //             o_mesh.triangles.push_back(Triangle(v0, v2, v1));
                
    //             // Second triangle of the face
    //             o_mesh.triangles.push_back(Triangle(v1, v2, v3));
    //         }
    //     }
    // }


    
    //     3           7

    // 1           5

    //     2           6

    // 0           4

    //On part dans le sens contraire d'une montre (anti clockwise)
    o_mesh.triangles.push_back(Triangle(5, 1, 0));
    o_mesh.triangles.push_back(Triangle(5, 0, 4));
    o_mesh.triangles.push_back(Triangle(1, 3, 2));
    o_mesh.triangles.push_back(Triangle(1, 2, 0));
    o_mesh.triangles.push_back(Triangle(3, 7, 6));
    o_mesh.triangles.push_back(Triangle(3, 6, 2));
    o_mesh.triangles.push_back(Triangle(7, 5, 4));
    o_mesh.triangles.push_back(Triangle(7, 4, 6));

    //Haut
    o_mesh.triangles.push_back(Triangle(7, 3, 1));
    o_mesh.triangles.push_back(Triangle(7, 1, 5));

    //Bas
    o_mesh.triangles.push_back(Triangle(4, 0, 2));
    o_mesh.triangles.push_back(Triangle(4, 2, 6));
    


}


// To complete for Exercice 2
void setTesselatedSquare(Mesh &o_mesh, int nX, int nY)
{
    o_mesh.vertices.clear();
    o_mesh.normals.clear();
    o_mesh.triangles.clear();
    o_mesh.colors.clear();

    std::vector<Vec3> tempNormal(nX * nY);

    for (int x = 0; x < nX; x++)
    {
        for (int y = 0; y < nY; y++)
        {
            float z = noise.GetNoise((float)x*noiseX, (float)y*noiseY);
            o_mesh.vertices.push_back(Vec3(x, y, z));
        }
    }

    for (int x = 0; x < nX - 1; x++){
        for (int y = 0; y < nY - 1; y++)
        {
            // Indices des sommets du carré actuel
            int p0 = x * nY + y;
            int p1 = p0 + 1;
            int p2 = (x + 1) * nY + y;
            int p3 = p2 + 1;

            
            o_mesh.triangles.push_back(Triangle(p0, p2, p1));
            o_mesh.triangles.push_back(Triangle(p1, p2, p3));

            Vec3 v0 = o_mesh.vertices[p0];
            Vec3 v1 = o_mesh.vertices[p1];
            Vec3 v2 = o_mesh.vertices[p2];
            Vec3 v3 = o_mesh.vertices[p3];
            Vec3 v4 = v0 - v1;
            Vec3 v5 = v2 - v1;
            Vec3 t1 = normalize(crossProduct(v4, v5));
            tempNormal[p0] += t1;
            tempNormal[p1] += t1;
            tempNormal[p2] += t1;

            Vec3 v6 = v1 - v3;
            Vec3 v7 = v2 - v3;
            Vec3 t2 = normalize(crossProduct(v6, v7));
            tempNormal[p1] += t2;
            tempNormal[p2] += t2;
            tempNormal[p3] += t2;
        }
    }

   

    for(int i = 0; i < tempNormal.size(); i++){
        o_mesh.normals.push_back(normalize(tempNormal[i]));
    }

   
   

    for(int i = 0; i < o_mesh.vertices.size(); i++){
       
        if (abs(o_mesh.normals[i][2]) >= 0.99 && abs(o_mesh.normals[i][2]) <= 1.01) {
            o_mesh.colors.push_back(Vec3(0.0, 1.0, 0.0)); // Vert si la normale est verticale (z == 1 ou z == -1)
        } else {
            o_mesh.colors.push_back(Vec3(0.6, 0.3, 0.2)); // Brun sinon
        }


    }

   
} 


bool saveOFF( const std::string & filename ,
              std::vector< Vec3 > & i_vertices ,
              std::vector< Vec3 > & i_normals ,
              std::vector< Triangle > & i_triangles,
              bool save_normals = true ) {
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return false;
    }

    myfile << "OFF" << std::endl ;

    unsigned int n_vertices = i_vertices.size() , n_triangles = i_triangles.size();
    myfile << n_vertices << " " << n_triangles << " 0" << std::endl;

    for( unsigned int v = 0 ; v < n_vertices ; ++v ) {
        myfile << i_vertices[v][0] << " " << i_vertices[v][1] << " " << i_vertices[v][2] << " ";
        if (save_normals) myfile << i_normals[v][0] << " " << i_normals[v][1] << " " << i_normals[v][2] << std::endl;
        else myfile << std::endl;
    }
    for( unsigned int f = 0 ; f < n_triangles ; ++f ) {
        myfile << 3 << " " << i_triangles[f][0] << " " << i_triangles[f][1] << " " << i_triangles[f][2];
        myfile << std::endl;
    }
    myfile.close();
    return true;
}

void openOFF( std::string const & filename,
              std::vector<Vec3> & o_vertices,
              std::vector<Vec3> & o_normals,
              std::vector< Triangle > & o_triangles,
              bool load_normals = true )
{
    std::ifstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open())
    {
        std::cout << filename << " cannot be opened" << std::endl;
        return;
    }

    std::string magic_s;

    myfile >> magic_s;

    if( magic_s != "OFF" )
    {
        std::cout << magic_s << " != OFF :   We handle ONLY *.off files." << std::endl;
        myfile.close();
        exit(1);
    }

    int n_vertices , n_faces , dummy_int;
    myfile >> n_vertices >> n_faces >> dummy_int;

    o_vertices.clear();
    o_normals.clear();

    for( int v = 0 ; v < n_vertices ; ++v )
    {
        float x , y , z ;

        myfile >> x >> y >> z ;
        o_vertices.push_back( Vec3( x , y , z ) );

        if( load_normals ) {
            myfile >> x >> y >> z;
            o_normals.push_back( Vec3( x , y , z ) );
        }
    }

    o_triangles.clear();
    for( int f = 0 ; f < n_faces ; ++f )
    {
        int n_vertices_on_face;
        myfile >> n_vertices_on_face;

        if( n_vertices_on_face == 3 )
        {
            unsigned int _v1 , _v2 , _v3;
            myfile >> _v1 >> _v2 >> _v3;

            o_triangles.push_back(Triangle( _v1, _v2, _v3 ));
        }
        else if( n_vertices_on_face == 4 )
        {
            unsigned int _v1 , _v2 , _v3 , _v4;
            myfile >> _v1 >> _v2 >> _v3 >> _v4;

            o_triangles.push_back(Triangle(_v1, _v2, _v3 ));
            o_triangles.push_back(Triangle(_v1, _v3, _v4));
        }
        else
        {
            std::cout << "We handle ONLY *.off files with 3 or 4 vertices per face" << std::endl;
            myfile.close();
            exit(1);
        }
    }

}


// ------------------------------------

void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    glEnable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);

    display_normals = false;
    display_unit_sphere = false;
    display_unit_cylinder = false;
    display_unit_cone = false;
    display_unit_cube = true;
    display_loaded_mesh = true;
    display_tesselation = false;
}




// ------------------------------------
// rendering.
// ------------------------------------


void drawVector( Vec3 const & i_from, Vec3 const & i_to ) {

    glBegin(GL_LINES);
    glVertex3f( i_from[0] , i_from[1] , i_from[2] );
    glVertex3f( i_to[0] , i_to[1] , i_to[2] );
    glEnd();
}

void drawTriangleMesh( Mesh const & i_mesh ) {
    glBegin(GL_TRIANGLES);
    bool okNormals = (i_mesh.normals.size() == i_mesh.vertices.size());
    bool okColors = (i_mesh.colors.size() == i_mesh.vertices.size());

    Vec3 p0, p1, p2;
    Vec3 n0, n1, n2;
    Vec3 c0 = Vec3(1, 1, 1), c1 = Vec3(1, 1, 1), c2 = Vec3(1, 1, 1);
    for(unsigned int tIt = 0 ; tIt < i_mesh.triangles.size(); ++tIt) {
        p0 = i_mesh.vertices[i_mesh.triangles[tIt][0]];
        p1 = i_mesh.vertices[i_mesh.triangles[tIt][1]];
        p2 = i_mesh.vertices[i_mesh.triangles[tIt][2]];

        if (okNormals) {
            n0 = i_mesh.normals[i_mesh.triangles[tIt][0]];
            n1 = i_mesh.normals[i_mesh.triangles[tIt][1]];
            n2 = i_mesh.normals[i_mesh.triangles[tIt][2]];
        }
        if (okColors) {
            c0 = i_mesh.colors[i_mesh.triangles[tIt][0]];
            c1 = i_mesh.colors[i_mesh.triangles[tIt][1]];
            c2 = i_mesh.colors[i_mesh.triangles[tIt][2]];
        }

        glNormal3f( n0[0] , n0[1] , n0[2] );
        glColor3f( c0[0], c0[1], c0[2]);
        glVertex3f( p0[0] , p0[1] , p0[2] );
        
        glNormal3f( n1[0] , n1[1] , n1[2] );
        glColor3f( c1[0], c1[1], c1[2]);
        glVertex3f( p1[0] , p1[1] , p1[2] );
        
        glNormal3f( n2[0] , n2[1] , n2[2] );
        glColor3f( c2[0], c2[1], c2[2]);
        glVertex3f( p2[0] , p2[1] , p2[2] );
    }
    glEnd();

    if(display_normals){
        glLineWidth(1.);
        glColor3f(1.,0.,0.);
        for(unsigned int pIt = 0 ; pIt < i_mesh.normals.size() ; ++pIt) {
            Vec3 to = i_mesh.vertices[pIt] + 0.02*i_mesh.normals[pIt];
            drawVector(i_mesh.vertices[pIt], to);
        }

    }

}


void draw () {

    if( display_unit_sphere ){
        glColor3f(0.8,1,0.8);
        drawTriangleMesh(unit_sphere);
    }

    if( display_unit_cylinder ){
        glColor3f(0.8,1,0.8);
        drawTriangleMesh(unit_cylinder);
    }

    if( display_unit_cone ){
        glColor3f(0.8,1,0.8);
        drawTriangleMesh(unit_cone);
    }

    if( display_unit_cube ){
        glColor3f(0.8,1,0.8);
        drawTriangleMesh(unit_cube);
    }

    if( display_tesselation ){
        glColor3f(0.8,1,0.8);
        drawTriangleMesh(tesselation);
    }

    if( display_loaded_mesh ){
        glColor3f(0.8,0.8,1);
        drawTriangleMesh(mesh);
    }


}


void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
}

void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;


    case 'w':
        GLint polygonMode[2];
        glGetIntegerv(GL_POLYGON_MODE, polygonMode);
        if(polygonMode[0] != GL_FILL)
            glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        break;


    case 'n': //Press n key to display normals
        display_normals = !display_normals;
        break;

    case '1': //Toggle loaded mesh display
        display_loaded_mesh = !display_loaded_mesh;
        break;

    case '2': //Toggle unit sphere mesh display
        display_unit_sphere = !display_unit_sphere;
        break;

    case '3': //Toggle tesselation
        display_tesselation = !display_tesselation;
        break;
    
    case '4': //Toggle unit cone mesh display
        display_unit_cone = !display_unit_cone;
        break;

    case '5': //Toggle unit cube mesh display
        display_unit_cube = !display_unit_cube;
        break;
    
    case '6': //Toggle unit cylinder mesh display
        display_unit_cylinder = !display_unit_cylinder;
        break;
    
    case '+':
        nX += 1;
        nY += 1;
        setUnitSphere(unit_sphere, nX, nY);
        setUnitCylinder(unit_cylinder, nX);
        setUnitCone(unit_cone, nX);
        setUnitCube(unit_cube);
        break;
    
    case '-':
        nX -= 1;
        nY -= 1;
        setUnitSphere(unit_sphere, nX, nY);
        setUnitCylinder(unit_cylinder, nX);
        setUnitCone(unit_cone, nX);
        setUnitCube(unit_cube);
        break;

    case 'a':
        noiseX--; 
        setTesselatedSquare(tesselation, nX, nY);
        break;

    case 'd':
        noiseX++; 
        setTesselatedSquare(tesselation, nX, nY);
        break;

    case 'z':
        noiseY++; 
        setTesselatedSquare(tesselation, nX, nY);
        break;

    case 's':
        noiseY--; 
        setTesselatedSquare(tesselation, nX, nY);
        break;

    default:
        break;
    }
    idle ();
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}



int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("TP HAI714I");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);

    noise.SetFractalOctaves(10);
    noise.SetFractalType(FastNoiseLite::FractalType::FractalType_FBm);
    noise.SetFractalGain(0.5);
    noise.SetFractalLacunarity(1.8f);

    //Unit sphere mesh loaded with precomputed normals
    //openOFF("data/unit_sphere_n.off", mesh.vertices, mesh.normals, mesh.triangles);

    setUnitSphere( unit_sphere, nX, nY );
    setUnitCylinder( unit_cylinder, nX );
    setUnitCone( unit_cone, nX );
    setUnitCube( unit_cube );

    setTesselatedSquare(tesselation, nX, nY);

    //Uncomment to see other meshes
    //openOFF("data/elephant_n.off", mesh.vertices, mesh.normals, mesh.triangles);

    glutMainLoop ();
    return EXIT_SUCCESS;
}

