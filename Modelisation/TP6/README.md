# Animation 

## Utilisation des boutons
- **Drop** : Ce bouton permet de voir la balle tomber et s'arrêter au niveau du sol. 
- **Squash & Strech** : Permet de voir les déformations du "strech and squash". La balle va s'étendre jusqu'à au sol et puis s'écraser
- **Slider** : Après avoir lancé une des deux animations **Drop** ou **Squash & Strech** , le slider va permettre de rembobiner les positions de la balle selon les animations. 
- **Loop** : Permet de lancer en boucle l'animation du **Squash & Strech**. 
- **Stop** : Stoppe l'animation en cours et remet les paramètres à zéro. 

Vous pouvez également remarquer le framerate en haut à gauche de votre écran. 