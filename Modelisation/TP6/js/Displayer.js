class Ball {
    x;
    y;
    w;
    sx; 
    sy; 

    constructor(x, y, w) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.sx = 1; 
        this.sy = 1; 
    }
}

class Displayer {
    static colors = [
        "#41566E",  // bleu
        "#F44",     // orange-rose
        "#FEE",
        "#C94",     // marron
        "#FFF",
        "#AFF",     // turquoise
        "#ffd800",  // jaune
    ];

    constructor(_htmlElement) {
        this.canvas = document.createElement("canvas");
        this.canvas.setAttribute("width", 800);
        this.canvas.setAttribute("height", 600);
        _htmlElement.appendChild(this.canvas);

        if (this.canvas.getContext) {
            this.g2d = this.canvas.getContext("2d");
        } else {
            this.canvas.write(
                "Votre navigateur ne peut visualiser cette page correctement"
            );
        }

        this.getCanvas().addEventListener("mousedown", this.mousePressed);

        this.wbg = Displayer.colors[0]; //  window background
        this.obg = Displayer.colors[1]; //  object background
        this.fg = Displayer.colors[2];  //  fg foreground
        this.lc = Displayer.colors[3];  //  line color
        this.lineWidth = 1;
        this.init();
    }


    mousePressed(e){
        console.log("mousePressed ", e); 
    }
    getCanvas() {
        return this.canvas;
    }

    init() {
        this.g2d.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.g2d.fillStyle = this.wbg;
        this.g2d.fillRect(0, 0, this.canvas.width, this.canvas.height);
    }

    //dessin en coordonnées canvas
    drawBall(p) {
        let x = p.x,
            y = p.y,
            w = p.w,
            sx = p.sx, 
            sy = p.sy; 

        this.g2d.beginPath();
        // this.g2d.arc(x, y, w, 0, Math.PI * 2, true);
        this.g2d.ellipse(x, y, w*sx, w*sy,0, 0, Math.PI *2, true); 
        this.g2d.fillStyle = this.obg;
        this.g2d.fill();
        this.g2d.strokeStyle = this.fg;
        this.g2d.stroke();
    }

    // dessin ligne en coordonnées canvas
    drawLine(x1, y1, x2, y2) {
        this.g2d.strokeStyle = this.lc;
        this.g2d.beginPath();
        this.g2d.moveTo(x1, y1);
        this.g2d.lineTo(x2, y2);
        this.g2d.stroke();
    }
}
