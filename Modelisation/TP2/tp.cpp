// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"
#include "src/FastNoiseLit.h"



// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 1600;
static unsigned int SCREENHEIGHT = 900;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;
static FastNoiseLite noise;


float factorielle(float n)
{
    float ret = 1;
    while (n > 1)
        ret *= n--;
    return ret;
}



//Fonction pour tracer une courbe par un ensemble de point
void DrawCurve(std::vector< Vec3 > TabPointsOfCurve, float nbPoint){
    
    glBegin(GL_LINE_STRIP); 
        
        for (int i = 0; i < nbPoint; i++){
            
            glVertex3f(TabPointsOfCurve[i][0], TabPointsOfCurve[i][1], TabPointsOfCurve[i][2]);
        }

    glEnd();

}

std::vector< Vec3 > HermiteCubicCurve(Vec3 P0, Vec3 P1, Vec3 V0, Vec3 V1, long nbU){
    //nbU = nb de point

    std::vector< Vec3 > points; 

    for (float i = 0; i < nbU; i++){
        float u = i/(nbU-1);
        float u3 = u*u*u; 
        float u2 = u*u; 

        float f1 = 2*u3 - 3*u2 + 1;
        float f2 = -2*u3 + 3*u2;
        float f3 = u3 - 2*u2 + u;
        float f4 = u3 - u2;

        Vec3 point = Vec3(f1*P0 + f2*P1 + f3*V0 + f4*V1);

        points.push_back(point);
     
    }

    return points;
    
}

void HermiteCubic() {

    Vec3 p20 = Vec3(0,0,0);
    Vec3 p21 = Vec3(2,0,0);
    Vec3 v20 = Vec3(1,1,0);
    Vec3 v21 = Vec3(1,-1,0);
    long nbU = 50;
    DrawCurve(HermiteCubicCurve(p20,p21,v20,v21,nbU), nbU);
}

float Bni(float n , float i, float u){
   
    return (factorielle(n) / (factorielle(i) * factorielle(n-i))) * pow(u, i) * pow(1-u, n-i); 

}

std::vector< Vec3 > BezierCurveByBernstrein(std::vector< Vec3 > TabControlPoint, long nbControlPoint, long nbU){
    std::vector< Vec3 > points; 
    
    for (float i = 0; i < nbU; i++){
       
        Vec3 p = Vec3(0, 0, 0); 
        float u = i/(nbU-1);
        for (float j = 0; j < nbControlPoint; j++){

            //la courbe est d'ordre n+1 et de degré n donc on a nbControlPoint-1 = n
        
            p += (Bni(nbControlPoint-1, j, u) * TabControlPoint[j]); 
        }

        points.push_back(p); 

    }

    return points;

}

int inter = 0;
int nbPoints = 0; 

std::vector<std::vector<Vec3>> InterPoints1;

Vec3 deCasteljau(std::vector<Vec3> points, long nbControlPoint, float u) {
    if (nbControlPoint == 1) {

        return points[0];

    } else {

        std::vector<Vec3> newPoints;
        std::vector<Vec3> intermed;
        intermed.clear(); 
       
        for (long i = 0; i < nbControlPoint -1; i++) {
            float x = (1.0f - u) * points[i][0] + u * points[i + 1][0];
            float y = (1.0f - u) * points[i][1] + u * points[i + 1][1];
            float z = (1.0f - u) * points[i][2] + u * points[i + 1][2];
            
            newPoints.push_back(Vec3(x, y, z)); 
        }

        
        if (inter == 0 || inter == nbPoints/3) {
            
            for (long i = 0; i < nbControlPoint-1; i++) {
                float x = (1.0f - u) * points[i][0] + u * points[i + 1][0];
                float y = (1.0f - u) * points[i][1] + u * points[i + 1][1];
                float z = (1.0f - u) * points[i][2] + u * points[i + 1][2];
                
                intermed.push_back(Vec3(x, y, z)); 
            }
            
            
            
            intermed.push_back(points[nbControlPoint-1]);
            InterPoints1.push_back(intermed);
           
        }
        
        return deCasteljau(newPoints, nbControlPoint - 1, u);

    }
}

std::vector<Vec3> BezierCurveByCasteljau(std::vector<Vec3> TabControlPoint, long nbControlPoint, long nbU) {
    std::vector<Vec3> curvePoints;
    nbPoints = nbU; 

    for (float i = 0; i < nbU; i++) {
        
        float u = i / (nbU - 1);
        Vec3 point = deCasteljau(TabControlPoint, nbControlPoint, u);
        curvePoints.push_back(point);
        inter++;
    }
    
    return curvePoints;
}


void DrawBezierCurveByBernstrein() {

    std::vector< Vec3 > TabControlPoint;
    TabControlPoint.push_back(Vec3(0,0,0));
    TabControlPoint.push_back(Vec3(-0.5,1,0));
    TabControlPoint.push_back(Vec3(-1.5,0,0));
    TabControlPoint.push_back(Vec3(-0.5,-1,0));
    TabControlPoint.push_back(Vec3(0,-1.5,0));
    TabControlPoint.push_back(Vec3(0.75,-1.75,0));
    TabControlPoint.push_back(Vec3(1.5,-0.75,0));
    TabControlPoint.push_back(Vec3(1,0,0));
    TabControlPoint.push_back(Vec3(-1, 3, 0));
    TabControlPoint.push_back(Vec3(-1.5, 3.5, 0));
    TabControlPoint.push_back(Vec3(-0.5, 4.5, 0));
    TabControlPoint.push_back(Vec3(1, 3, 0));
 
    //affichage des points de controle

    glColor3f(1.0f, 0.0f, 0.0f);
    glPointSize(5.0);

    glBegin(GL_LINE_STRIP);
    
    for(float i = 0; i< TabControlPoint.size();i++){
        
        glVertex3f(TabControlPoint[i][0], TabControlPoint[i][1], TabControlPoint[i][2]);
    }
    glEnd();


    long nbU = 50;
    glColor3f(0.0f, 1.0f, 0.0f);
    DrawCurve(BezierCurveByBernstrein(TabControlPoint, TabControlPoint.size(), nbU), nbU);

    
}


void DrawBezierCurveByCasteljau() {

    std::vector< Vec3 > TabControlPoint;
    TabControlPoint.push_back(Vec3(0,0,0));
    TabControlPoint.push_back(Vec3(-0.5,1,0));
    TabControlPoint.push_back(Vec3(-1.5,0,0));
    TabControlPoint.push_back(Vec3(-0.5,-1,0));
    TabControlPoint.push_back(Vec3(0,-1.5,0));
    TabControlPoint.push_back(Vec3(0.75,-1.75,0));
    TabControlPoint.push_back(Vec3(1.5,-0.75,0));
    TabControlPoint.push_back(Vec3(1,0,0));
    TabControlPoint.push_back(Vec3(-1, 3, 0));
    TabControlPoint.push_back(Vec3(-1.5, 3.5, 0));
    TabControlPoint.push_back(Vec3(-0.5, 4.5, 0));
    TabControlPoint.push_back(Vec3(1, 3, 0));
 
    //affichage des points de controle

    glColor3f(1.0f, 0.0f, 0.0f);
    glPointSize(5.0);

    glBegin(GL_POINTS);
    
    for(float i = 0; i< TabControlPoint.size();i++){
        
        glVertex3f(TabControlPoint[i][0], TabControlPoint[i][1], TabControlPoint[i][2]);
    }
    glEnd();


    long nbU = 50;
    glColor3f(0.0f, 1.0f, 0.0f);
    
    DrawCurve(BezierCurveByCasteljau(TabControlPoint, TabControlPoint.size(), nbU), nbU);
    

    for (float i = 0; i < InterPoints1.size(); i++){
        glColor3f(0.0f, 0.0f, 1.0f);
        DrawCurve(InterPoints1[i], InterPoints1[i].size()); 

    }


    
}


// ------------------------------------

void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    //glEnable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
}


// ------------------------------------
// rendering.
// ------------------------------------

void draw () {

    //HermiteCubic();
    //DrawBezierCurveByBernstrein();
    DrawBezierCurveByCasteljau();

}


void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
}

void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;
    default:
        break;
    }
    idle ();
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}



int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("TP HAI714I");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);

    noise.SetFractalOctaves(10);
    noise.SetFractalType(FastNoiseLite::FractalType::FractalType_FBm);
    noise.SetFractalGain(0.5);
    noise.SetFractalLacunarity(1.8f);


    glutMainLoop ();
    return EXIT_SUCCESS;
}
