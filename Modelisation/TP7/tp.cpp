// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"
#include "src/FastNoiseLit.h"

int indiceImage(int x, int y, int w) {
    return y*w + x;
}

Vec3 normalize(Vec3 vec){
    float length = vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]; 
    length = std::sqrt(length); 

    return vec/length; 
}
Vec3 crossProduct(Vec3& a, Vec3& b) {
    return Vec3(
        a[1] * b[2] - a[2] * b[1],
        a[2] * b[0] - a[0] * b[2],
        a[0] * b[1] - a[1] * b[0]
    );
}

struct Triangle {
    inline Triangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline Triangle (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline Triangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    unsigned int & operator [] (unsigned int iv) { return v[iv]; }
    unsigned int operator [] (unsigned int iv) const { return v[iv]; }
    inline virtual ~Triangle () {}
    inline Triangle & operator = (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    // membres :
    unsigned int v[3];
};


struct Mesh {
    std::vector< Vec3 > vertices;
    std::vector< Vec3 > normals;
    std::vector< Triangle > triangles;
    
};

Mesh VoxelBase;

//Meshes to generate
Mesh VoxelSphere;
Mesh VoxelCylinder;
Mesh VoxelMesh; 


// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 1600;
static unsigned int SCREENHEIGHT = 900;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;


void DisplayVoxelBase(Mesh & o_mesh, Vec3 centre, double length){
    o_mesh.normals.clear(); 
    o_mesh.vertices.clear(); 
    o_mesh.triangles.clear(); 
   
    std::vector<Vec3> points; 
    float dist = length/2; 
    
    float x = centre[0]; 
    float y = centre[1]; 
    float z = centre[2]; 

    Vec3 v0 = Vec3(x-dist, y+dist, z-dist); 
    Vec3 v1 = Vec3(x+dist, y+dist, z-dist); 
    Vec3 v2 = Vec3(x+dist, y-dist, z-dist); 
    Vec3 v3 = Vec3(x-dist, y-dist, z-dist); 

    Vec3 v4 = Vec3(x-dist, y+dist, z+dist); 
    Vec3 v5 = Vec3(x+dist, y+dist, z+dist); 
    Vec3 v6 = Vec3(x+dist, y-dist, z+dist); 
    Vec3 v7 = Vec3(x-dist, y-dist, z+dist);

    // FRONT
    o_mesh.vertices.push_back(v2); 
    o_mesh.vertices.push_back(v1);
    o_mesh.vertices.push_back(v0);
    o_mesh.vertices.push_back(v3);


    // BACK
    o_mesh.vertices.push_back(v6);
    o_mesh.vertices.push_back(v5);
    o_mesh.vertices.push_back(v4);
    o_mesh.vertices.push_back(v7);   

    // RIGHT
    o_mesh.vertices.push_back(v6);
    o_mesh.vertices.push_back(v5);
    o_mesh.vertices.push_back(v1);
    o_mesh.vertices.push_back(v2); 

    // LEFT
    o_mesh.vertices.push_back(v0);
    o_mesh.vertices.push_back(v4);
    o_mesh.vertices.push_back(v7);
    o_mesh.vertices.push_back(v3);


}


void DisplayVoxel(Mesh & o_mesh, Vec3 centre, double length){
    o_mesh.normals.clear(); 
    o_mesh.vertices.clear(); 
    o_mesh.triangles.clear(); 
   
    std::vector<Vec3> points; 
    float dist = length/2; 
    
    float x = centre[0]; 
    float y = centre[1]; 
    float z = centre[2]; 

    Vec3 v0 = Vec3(x-dist, y+dist, z-dist); 
    Vec3 v1 = Vec3(x+dist, y+dist, z-dist); 
    Vec3 v2 = Vec3(x+dist, y-dist, z-dist); 
    Vec3 v3 = Vec3(x-dist, y-dist, z-dist); 

    Vec3 v4 = Vec3(x-dist, y+dist, z+dist); 
    Vec3 v5 = Vec3(x+dist, y+dist, z+dist); 
    Vec3 v6 = Vec3(x+dist, y-dist, z+dist); 
    Vec3 v7 = Vec3(x-dist, y-dist, z+dist);

    o_mesh.vertices.push_back(v0); 
    o_mesh.vertices.push_back(v1);
    o_mesh.vertices.push_back(v2);
    o_mesh.vertices.push_back(v3);
    o_mesh.vertices.push_back(v4);
    o_mesh.vertices.push_back(v5);
    o_mesh.vertices.push_back(v6);
    o_mesh.vertices.push_back(v7);   

    
    // FRONT
    o_mesh.triangles.push_back(Triangle(2, 1, 0));
    o_mesh.triangles.push_back(Triangle(2, 0, 3));
    // BACK
    o_mesh.triangles.push_back(Triangle(6, 5, 4));
    o_mesh.triangles.push_back(Triangle(6, 4, 7));
    // RIGHT
    o_mesh.triangles.push_back(Triangle(6, 5, 1));
    o_mesh.triangles.push_back(Triangle(6, 1, 2));
    // LEFT
    o_mesh.triangles.push_back(Triangle(3, 0, 4));
    o_mesh.triangles.push_back(Triangle(3, 4, 7));
    // TOP
    o_mesh.triangles.push_back(Triangle(1, 5, 4));
    o_mesh.triangles.push_back(Triangle(1, 4, 0));
    // BOTTOM
    o_mesh.triangles.push_back(Triangle(2, 6, 7));
    o_mesh.triangles.push_back(Triangle(2, 7, 3));

    for(unsigned int i = 0; i < o_mesh.triangles.size(); i++){

        Vec3 p0 = o_mesh.vertices[o_mesh.triangles[i][0]]; 
        Vec3 p1 = o_mesh.vertices[o_mesh.triangles[i][1]]; 
        Vec3 p2 = o_mesh.vertices[o_mesh.triangles[i][2]]; 

        Vec3 v1 = p0 - p1; 
        Vec3 v2 = p2 - p1; 
        Vec3 N = Vec3::cross(v1, v2);
        N.normalize();
        o_mesh.normals.push_back(N); 
         

    }

    

}



void drawVoxel(Mesh & o_mesh){
    // Initialisation du mesh pour dessiner le Voxel
   
    Vec3 p0, p1, p2;
    

    for(unsigned int i = 0; i < o_mesh.triangles.size(); i++){
        glBegin(GL_TRIANGLES);
            p0 = o_mesh.vertices[o_mesh.triangles[i][0]];
            p1 = o_mesh.vertices[o_mesh.triangles[i][1]];
            p2 = o_mesh.vertices[o_mesh.triangles[i][2]];
            // METTRE LES NORMALES AVANT CAR SEQUENTIEL
            glNormal3f( o_mesh.normals[i][0], o_mesh.normals[i][1],o_mesh.normals[i][2]);
            glVertex3f( p0[0] , p0[1] , p0[2] );
            glVertex3f( p1[0] , p1[1] , p1[2] );
            glVertex3f( p2[0] , p2[1] , p2[2] );
        

            
        glEnd();
            

    }
   


}
void drawVoxelBase(){
    // Dessiner le Voxel de base
    for (unsigned int i = 0; i < VoxelBase.vertices.size(); i += 4) {
        glBegin(GL_LINE_LOOP);
            glVertex3f(VoxelBase.vertices[i][0], VoxelBase.vertices[i][1], VoxelBase.vertices[i][2]);
            glVertex3f(VoxelBase.vertices[i + 1][0], VoxelBase.vertices[i + 1][1], VoxelBase.vertices[i + 1][2]);
            glVertex3f(VoxelBase.vertices[i + 2][0], VoxelBase.vertices[i + 2][1], VoxelBase.vertices[i + 2][2]);
            glVertex3f(VoxelBase.vertices[i + 3][0], VoxelBase.vertices[i + 3][1], VoxelBase.vertices[i + 3][2]);
        glEnd();
    }
   

   
}

// Vérifie si le points est dans la sphere
bool inSphere(Vec3 point, Vec3 centre, double rayon) {
    
    // distance euclidienne entre 2 points
    if (pow(point[0]-centre[0], 2) + pow(point[1]-centre[1], 2) + pow(point[2]-centre[2], 2) < pow(rayon, 2)) {
        return true;
    }

    return false;
}



// SANS OCTREE
void DisplaySphereVolumicNoOctree(Mesh & o_mesh, Vec3 centre, double rayon, double resolution) {
    // On choisi de manière arbitraire de partir sur une voxel de base centre 0 et de rayon 1
    float length = 1;
    for (int i = 1; i < resolution; i++) {
        length = (float)length/2;
    }


    int posX = 0;
    int posY = 0;
    int posZ = 0;
    float voxX;
    float voxY;
    float voxZ;

    if(resolution == 1) {
        if (inSphere(Vec3(0, 0, 0), centre, rayon)) {
            DisplayVoxel(o_mesh, Vec3(0, 0, 0), length);
            drawVoxel(o_mesh);
        }
    }
    else { 
        
        for (float i = 0; i < 1; i += length) {
            // Calcul de la position du voxel en X : length/2 -> subdivision et posX * length -> permet de connaitre le position du cube 
            voxX = length/2 + posX*length;
            posY = 0;

            for (float j = 0; j < 1; j+=length) {
                voxY = length/2 + posY*length;
                posZ = 0;

                for (float k = 0; k < 1; k+=length) {
                    voxZ = length/2 + posZ*length;

                    if (inSphere(Vec3(voxX, voxY, voxZ), centre, rayon)) {
                        DisplayVoxel(o_mesh, Vec3(voxX, voxY, voxZ), length);
                        drawVoxel(o_mesh);

                        DisplayVoxel(o_mesh, Vec3(-voxX, -voxY, voxZ), length);
                        drawVoxel(o_mesh);

                        DisplayVoxel(o_mesh, Vec3(voxX, -voxY, voxZ), length);
                        drawVoxel(o_mesh);

                        DisplayVoxel(o_mesh, Vec3(-voxX, voxY, voxZ), length);
                        drawVoxel(o_mesh);

                        DisplayVoxel(o_mesh, Vec3(voxX, voxY, -voxZ), length);
                        drawVoxel(o_mesh);

                        DisplayVoxel(o_mesh, Vec3(-voxX, -voxY, -voxZ), length);
                        drawVoxel(o_mesh);

                        DisplayVoxel(o_mesh, Vec3(voxX, -voxY, -voxZ), length);
                        drawVoxel(o_mesh);

                        DisplayVoxel(o_mesh, Vec3(-voxX, voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                    }
                    posZ++;
                }
                posY++;
            }
            posX++;
        }
    }
}

bool inCylinder(Vec3 point, Vec3 axisOrigin, Vec3 axisVector, float rayon) {
    // Axis Vector = direction 
    // Axis Origin = point d'origine
    Vec3 axisUp = axisOrigin + axisVector;

    if (Vec3::dot(point - axisOrigin, axisVector) >= 0 && Vec3::dot(point - axisUp, axisVector) <= 0 && (Vec3::cross(point - axisOrigin, axisVector)).length() <= rayon) {
        return true;
    }
    return false;

}

void DisplayCylinderVolumic(Mesh & o_mesh, Vec3 axisOrigin, Vec3 axisVector, double rayon, double resolution) {
    float length = 1;
    for (int i = 1; i < resolution; i++) {
        length = (float)length/2;
    }

    

    int posX = 0;
    int posY = 0;
    int posZ = 0;
    float voxX;
    float voxY;
    float voxZ;

    if(resolution == 1) {
        if (inCylinder(Vec3(0, 0, 0), axisOrigin, axisVector, rayon)) {
            DisplayVoxel(o_mesh, Vec3(0, 0, 0), length);
            drawVoxel(o_mesh);
        }
    }
    else {
        for (float i = 0; i < 0.5; i += length) {
            voxX = length/2 + posX*length;
            posY = 0;

            for (float j = 0; j < 0.5; j+=length) {
                voxY = length/2 + posY*length;
                posZ = 0;

                for (float k = 0; k < 0.5; k+=length) {
                    voxZ = length/2 + posZ*length;

                    if (inCylinder(Vec3(voxX, voxY, voxZ), axisOrigin, axisVector, rayon)) {
                       
                        DisplayVoxel(o_mesh,Vec3(voxX, voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, -voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, -voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, -voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, -voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                    }
                    posZ++;
                }
                posY++;
            }
            posX++;
        }
    }
}

void Display_INTERSECTION_SphereCylinder(Mesh & o_mesh, Vec3 centreSphere, double rayonSphere, Vec3 axisOriginCylinder, Vec3 axisVectorCylinder, double rayonCylinder, double resolution) {
    float length = 1;
    for (int i = 1; i < resolution; i++) {
        length = (float)length/2;
    }

    

    int posX = 0;
    int posY = 0;
    int posZ = 0;
    float voxX;
    float voxY;
    float voxZ;

    if(resolution == 1) {
        if (inCylinder(Vec3(0, 0, 0), axisVectorCylinder, axisOriginCylinder, rayonCylinder) && inSphere(Vec3(0, 0, 0), centreSphere, rayonSphere)) {
            DisplayVoxel(o_mesh,Vec3(0, 0, 0), length);
            drawVoxel(o_mesh);
        }
    }
    else {
        for (float i = 0; i < 0.5; i += length) {
            voxX = length/2 + posX*length;
            posY = 0;

            for (float j = 0; j < 0.5; j+=length) {
                voxY = length/2 + posY*length;
                posZ = 0;

                for (float k = 0; k < 0.5; k+=length) {
                    voxZ = length/2 + posZ*length;

                    if (inCylinder(Vec3(voxX, voxY, voxZ), axisOriginCylinder, axisVectorCylinder, rayonCylinder) && inSphere(Vec3(voxX, voxY, voxZ), centreSphere, rayonSphere)) {
                        DisplayVoxel(o_mesh,Vec3(voxX, voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, -voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, -voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, -voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, -voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                    }
                    posZ++;
                }
                posY++;
            }
            posX++;
        }
    }
}

void Display_SOUSTRACTION_SphereCylinder(Mesh & o_mesh, Vec3 centreSphere, double rayonSphere, Vec3 axisOriginCylinder, Vec3 axisVectorCylinder, double rayonCylinder, double resolution) {
    float length = 1;
    for (int i = 1; i < resolution; i++) {
        length = (float)length/2;
    }

    

    int posX = 0;
    int posY = 0;
    int posZ = 0;
    float voxX;
    float voxY;
    float voxZ;

    if(resolution == 1) {
        if (inCylinder(Vec3(0, 0, 0), axisVectorCylinder, axisOriginCylinder, rayonCylinder) && inSphere(Vec3(0, 0, 0), centreSphere, rayonSphere)) {
            DisplayVoxel(o_mesh,Vec3(0, 0, 0), length);
            drawVoxel(o_mesh);
        }
    }
    else {
        for (float i = 0; i < 0.5; i += length) {
            voxX = length/2 + posX*length;
            posY = 0;

            for (float j = 0; j < 0.5; j+=length) {
                voxY = length/2 + posY*length;
                posZ = 0;

                for (float k = 0; k < 0.5; k+=length) {
                    voxZ = length/2 + posZ*length;

                    if (inSphere(Vec3(voxX, voxY, voxZ), centreSphere, rayonSphere) && !inCylinder(Vec3(voxX, voxY, voxZ), axisOriginCylinder, axisVectorCylinder, rayonCylinder) ) {
                        DisplayVoxel(o_mesh,Vec3(voxX, voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, -voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, -voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, -voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, -voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                    }
                    posZ++;
                }
                posY++;
            }
            posX++;
        }
    }
}


void Display_UNION_SphereCylinder(Mesh & o_mesh, Vec3 centreSphere, double rayonSphere, Vec3 axisOriginCylinder, Vec3 axisVectorCylinder, double rayonCylinder, double resolution) {
    float length = 1;
    for (int i = 1; i < resolution; i++) {
        length = (float)length/2;
    }

    

    int posX = 0;
    int posY = 0;
    int posZ = 0;
    float voxX;
    float voxY;
    float voxZ;

    if(resolution == 1) {
        if (inCylinder(Vec3(0, 0, 0), axisVectorCylinder, axisOriginCylinder, rayonCylinder) && inSphere(Vec3(0, 0, 0), centreSphere, rayonSphere)) {
            DisplayVoxel(o_mesh,Vec3(0, 0, 0), length);
            drawVoxel(o_mesh);
        }
    }
    else {
        for (float i = 0; i < 0.5; i += length) {
            voxX = length/2 + posX*length;
            posY = 0;

            for (float j = 0; j < 0.5; j+=length) {
                voxY = length/2 + posY*length;
                posZ = 0;

                for (float k = 0; k < 0.5; k+=length) {
                    voxZ = length/2 + posZ*length;

                    if (inSphere(Vec3(voxX, voxY, voxZ), centreSphere, rayonSphere) || inCylinder(Vec3(voxX, voxY, voxZ), axisOriginCylinder, axisVectorCylinder, rayonCylinder) ) {
                        DisplayVoxel(o_mesh,Vec3(voxX, voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, -voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, -voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, voxY, voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, -voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(voxX, -voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                        DisplayVoxel(o_mesh,Vec3(-voxX, voxY, -voxZ), length);
                        drawVoxel(o_mesh);
                    }
                    posZ++;
                }
                posY++;
            }
            posX++;
        }
    }
}

int checkCell(Vec3 point, Vec3 centre, double rayon) {
    if (pow(point[0]-centre[0], 2) + pow(point[1]-centre[1], 2) + pow(point[2]-centre[2], 2) < pow(rayon, 2)) {
        return 1;
    }
    else if (pow(point[0]-centre[0], 2) + pow(point[1]-centre[1], 2) + pow(point[2]-centre[2], 2) == pow(rayon, 2)) {
        return 0;
    }

    return -1;
}




void DisplaySphereVolumicOctree(Mesh& o_mesh, Vec3 centre, double rayon, double resolution, Vec3 cellCenter, double cellSize) {
 
    int result = checkCell(cellCenter, centre, rayon); 


    if (result == 0 || resolution == 0) {
        // Afficher le voxel si la cellule intersecte la sphère
        DisplayVoxel(o_mesh, cellCenter, cellSize);
        drawVoxel(o_mesh);

    }  else if (result == 1) {
        // Si la cellule est entièrement à l'intérieur de la sphère, subdiviser récursivement
        double newCellSize = cellSize / 2.0;
       
        for (float x = -1; x <= 1; x += 2) {
            for (float y = -1; y <= 1; y += 2) {
                for (float z = -1; z <= 1; z += 2) {
                    
                    Vec3 newCellCenter; 
                    newCellCenter[0] = cellCenter[0] + (newCellSize /2 * x);  
                    newCellCenter[1] = cellCenter[1] + (newCellSize /2 * y);  
                    newCellCenter[2] = cellCenter[2] + (newCellSize /2 * z);  
                    // DisplayVoxel(o_mesh, newCellCenter, newCellSize);
                    // drawVoxel(o_mesh);
                    // DisplayVoxelBase(VoxelBase, newCellCenter, newCellSize);
                    //  drawVoxelBase(); 
                    DisplaySphereVolumicOctree(o_mesh, centre, rayon, resolution - 1, newCellCenter, newCellSize);
                }
            }
        }
    }
}

void DisplaySphereVolumic(Mesh & o_mesh, Vec3 centre, double rayon, double resolution) {
    // Appel initial avec la cellule englobante
    
    double initialCellSize = rayon * 2; 
    Vec3 initialCellCenter = centre;  // La sphère est centrée à l'origine
    DisplaySphereVolumicOctree(o_mesh, centre, rayon, resolution, initialCellCenter, initialCellSize);
    // drawVoxel(o_mesh);  // Dessiner la sphère générée
}



void draw(){
    // DisplayVoxelBase(VoxelBase, Vec3(0, 0, 0), 1); 
    // drawVoxelBase(); 

    // DisplayVoxel(VoxelMesh, Vec3(0.25, 0.25, 0.25), 0.5); 
    // drawVoxel(VoxelMesh); 
    DisplaySphereVolumicNoOctree(VoxelSphere, Vec3(0, 0, 0), 0.5, 6);
    DisplaySphereVolumic(VoxelSphere, Vec3(1, 0, 0), 0.5, 6);
    // DisplayCylinderVolumic(VoxelCylinder, Vec3(0, 0, 0), Vec3(1, 0, 0), 0.25, 5);
    // DisplayCylinderVolumic(VoxelCylinder, Vec3(0, 0, 0), Vec3(0, 1, 0), 0.25, 7);

    // DisplayCylinderVolumic(VoxelCylinder, Vec3(0, 0, 0), Vec3(0, 1, 1), 0.25, 5);



    // Display_INTERSECTION_SphereCylinder(Vec3 centreSphere, double rayonSphere, Vec3 axisOriginCylinder, Vec3 axisVectorCylinder, double rayonCylinder, double résolution);
    // Display_INTERSECTION_SphereCylinder(VoxelMesh, Vec3(0, 0, 0), 0.5, Vec3(0, 0, 0), Vec3(0, 1, 0), 0.25, 5);
    // Display_INTERSECTION_SphereCylinder(VoxelMesh, Vec3(0, 0, 0), 0.25, Vec3(0, 0, 0), Vec3(0, 1, 0), 0.5, 5);

    // Appartient à la sphere mais pas au cylindre
    // Display_SOUSTRACTION_SphereCylinder(VoxelMesh, Vec3(0, 0, 0), 0.5, Vec3(0, 0, 0), Vec3(0, 1, 0), 0.25, 5);

    // Display_UNION_SphereCylinder(VoxelMesh, Vec3(0, 0, 0), 0.25, Vec3(0, 0, 0), Vec3(0, 1, 0), 0.1, 6);
    



}




bool saveOFF( const std::string & filename ,
              std::vector< Vec3 > & i_vertices ,
              std::vector< Vec3 > & i_normals ,
              std::vector< Triangle > & i_triangles,
              bool save_normals = true ) {
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return false;
    }

    myfile << "OFF" << std::endl ;

    unsigned int n_vertices = i_vertices.size() , n_triangles = i_triangles.size();
    myfile << n_vertices << " " << n_triangles << " 0" << std::endl;

    for( unsigned int v = 0 ; v < n_vertices ; ++v ) {
        myfile << i_vertices[v][0] << " " << i_vertices[v][1] << " " << i_vertices[v][2] << " ";
        if (save_normals) myfile << i_normals[v][0] << " " << i_normals[v][1] << " " << i_normals[v][2] << std::endl;
        else myfile << std::endl;
    }
    for( unsigned int f = 0 ; f < n_triangles ; ++f ) {
        myfile << 3 << " " << i_triangles[f][0] << " " << i_triangles[f][1] << " " << i_triangles[f][2];
        myfile << std::endl;
    }
    myfile.close();
    return true;
}

void openOFF( std::string const & filename,
              std::vector<Vec3> & o_vertices,
              std::vector<Vec3> & o_normals,
              std::vector< Triangle > & o_triangles,
              bool load_normals = true )
{
    std::ifstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open())
    {
        std::cout << filename << " cannot be opened" << std::endl;
        return;
    }

    std::string magic_s;

    myfile >> magic_s;

    if( magic_s != "OFF" )
    {
        std::cout << magic_s << " != OFF :   We handle ONLY *.off files." << std::endl;
        myfile.close();
        exit(1);
    }

    int n_vertices , n_faces , dummy_int;
    myfile >> n_vertices >> n_faces >> dummy_int;

    o_vertices.clear();
    o_normals.clear();

    for( int v = 0 ; v < n_vertices ; ++v )
    {
        float x , y , z ;

        myfile >> x >> y >> z ;
        o_vertices.push_back( Vec3( x , y , z ) );

        if( load_normals ) {
            myfile >> x >> y >> z;
            o_normals.push_back( Vec3( x , y , z ) );
        }
    }

    o_triangles.clear();
    for( int f = 0 ; f < n_faces ; ++f )
    {
        int n_vertices_on_face;
        myfile >> n_vertices_on_face;

        if( n_vertices_on_face == 3 )
        {
            unsigned int _v1 , _v2 , _v3;
            myfile >> _v1 >> _v2 >> _v3;

            o_triangles.push_back(Triangle( _v1, _v2, _v3 ));
        }
        else if( n_vertices_on_face == 4 )
        {
            unsigned int _v1 , _v2 , _v3 , _v4;
            myfile >> _v1 >> _v2 >> _v3 >> _v4;

            o_triangles.push_back(Triangle(_v1, _v2, _v3 ));
            o_triangles.push_back(Triangle(_v1, _v3, _v4));
        }
        else
        {
            std::cout << "We handle ONLY *.off files with 3 or 4 vertices per face" << std::endl;
            myfile.close();
            exit(1);
        }
    }

}


// ------------------------------------

void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    // glEnable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
}





void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw(); 
    // drawVoxelBase();
    // drawVoxel(); 
    glFlush ();
    glutSwapBuffers ();
    
}

void idle () {
    glutPostRedisplay ();
}

void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;


    case 'w':
        GLint polygonMode[2];
        glGetIntegerv(GL_POLYGON_MODE, polygonMode);
        if(polygonMode[0] != GL_FILL)
            glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        break;


    case 'v': //Press n key to display normals
      
        break;
    
    default:
        break;
    }
    idle ();
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}



int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("TP HAI714I");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);

    // Affiche la Voxel de base
    // DisplayVoxelBase(VoxelBase, Vec3(0, 0, 0), 1); 
   

   

    // DisplayVoxel(VoxelMesh, Vec3(0, 0, 0), 0.5); 

    glutMainLoop ();
    return EXIT_SUCCESS;
}

