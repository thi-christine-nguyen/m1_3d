# Readme

## Exercice 1 : Orientation de trois points dans le plan


#### 2. `detSign`
   - Retourne le signe du déterminant de deux vecteurs.
   - +1 si le déterminant est positif (tour gauche), -1 s'il est négatif (tour droit), 0 s'il est nul (aligné).

#### 3. `tour`
   - Détermine l'orientation de trois points.
   - Prend 3 points (origine, p1, p2) à partir de l'origine, construit les vecteurs et retourne le signe du déterminant des deux vecteur (voir `detSign`)

## Exercice 2 : Algorithme des demi-plans

#### `algoDemiPlan`
   - Calcule l'enveloppe convexe en utilisant l'algorithme des demi-plans.
   - Retourne un tableau de couple de points (le points actuel et son successeur dans le sens droit).
 

## Exercice 3 : Algorithme de Jarvis

#### `algoJarvis`
   - Calcule l'enveloppe convexe en utilisant l'algorithme de Jarvis.
   - La stratégie est de partir du point le plus bas (Y minimum) puis de le comparer avec les autres points et récupérer le point suivant le plus à droite. 
   - Retourne un tableau avec tous les points qui forme l'enveloppe convexe. 

## Exercice 6 : Comparaison empirique des performances

Implémentation de la fonction `measurePerformance` pour comparer les performances de l'algo des demi plans et l'algo de Jarvis

#### `measurePerformance`
   - Mesure le temps d'exécution d'un algorithme pour un ensemble donné de points.
   - Renvoie le temps en milliseconde.

**Résultats :**
   - Algorithme des demi-plans (O($n^3$)) :
      - 1000 points : 68 ms
      - 10000 points : 6544.1 ms
   - Algorithme de Jarvis (O($n$)) :
      - 1000 points : 1 ms
      - 10000 points : 5 ms

**Conclusion :**
   - Les résultats concordent avec les complexités théoriques des algorithmes.
   

