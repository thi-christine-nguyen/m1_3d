class Test {
    static defaultAction = "draw";
    cp; // control panel
    a;  // action
    d;  // displayer
    centre = new Coord2D(0, 0);
    referencePoint;
    states = { center: 2, comp: 3, smaller: -1, bigger: 1, equal: 0 };


    constructor(_action) {
        this.a = _action;
    }

    measurePerformance(points) {
        const start = performance.now();
        // EnveloppeConvexe.algoDemiPlan(points); 
        EnveloppeConvexe.algoJarvis(points); 
        const end = performance.now();
        return end - start;
    }

    //priori simplicité (version draft: les clés des options ui sont utilisées telles quelles dans un if else infernal, beurk mais rapide, à revoir ?)
    //ui keys = tour, comp, tri, algo_dp, algo_jarvis, algo_graham
    run() {
        this.d.init();
        this.d.setModelSpace(...this.getModelSpace());
        let points = this.getPoints(); 

        if(this.a == "tour"){
          
            for(let i = 0; i < points.length-2; i+=3){
                this.d.drawTour(Coord2D.tour(points[i], points[i+1], points[i+2]), [points[i], points[i+1], points[i+2]], true); 
            }

        
        }else if(this.a == "algo_demiPlan"){
            let result = EnveloppeConvexe.algoDemiPlan(points);

            for (const p of result){
                this.d.mDrawLine(p[0], p[1], true); 
            }
           
            // this.d.drawLines(result, true); 
            this.d.drawPoints(this.getPoints(), true);
           
            // console.log(this.measurePerformance(points)); 
            //1000 = 68 ms
            //10 000 = 6544.1 ms

                
        
        }else if (this.a == "algo_jarvis"){
            let result = EnveloppeConvexe.algoJarvis(points);
            
            this.d.drawLineLoop(EnveloppeConvexe.algoJarvis(points), true); 
            this.d.drawPoints(this.getPoints(), true);
            // console.log(this.measurePerformance(points)); 

            //1000 = 1 ms
            //10 000 = 5 ms
        }
        else{
            this.d.drawPoints(this.getPoints(), false);
        }

       

        //todo
    }

    setUI(_controlPanel, _displayer) {
        this.cp = _controlPanel;
        this.d = _displayer;
    }

    changeAction(_newA) {
        this.a = _newA;
        this.run();
    }
    
    refresh() {
        this.run();
    }

    // made to fit the signature of setModelSpace@Displayer(minX, minY, maxX, maxY)
    getModelSpace() {
        return [
            this.cp.getDataSet().minX,
            this.cp.getDataSet().minY,
            this.cp.getDataSet().maxX,
            this.cp.getDataSet().maxY
        ]
    }

    getPoints() {
        return this.cp.getDataSet().getPoints();
    }

    renewData() {
        this.cp.renewData();
        this.refresh();
    }
}