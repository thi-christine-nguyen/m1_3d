
class EnveloppeConvexe {
    static algo = { demiPlan: 0, jarvis: 1, graham: 2 };
    constructor(lesPoints, a) {
        this.algo = a ?? EnveloppeConvexe.algo.graham;
        this.points = lesPoints;
        this.envConv = new Array();
        this.runAlgo(this.algo);
    }

    getEnvConv() {
         return this.envConv;
    }

    updateEnvConvFromIndices(indices) {
       this.envConv = new Array();
       for (let i = 0; i < indices.length; i++) {
           this.envConv.push(this.points[indices[i]]);
        }      
    }

    runAlgo(idAlgo) {
        this.algo = idAlgo;
        switch (this.algo) {
            case EnveloppeConvexe.algo.demiPlan:
                this.envconv = this.algoDemiPlan(this.points);
                break;
            case EnveloppeConvexe.algo.jarvis:
                this.envconv = this.algoJarvis(this.points);
                break;
            case EnveloppeConvexe.algo.graham:
                this.envconv = this.algoGraham(this.points);
                break;
            default:
                console.log("algo non défini => algo jarvis utilisé");
                this.envconv = this.algoJarvis(this.points);
                break;
        }
    }

    static findFirst(i, j, points){
        for(let p = 0; p < points.length; p++){
            if(points[p] != points[i] && points[p] != points[j]){
                return p; 
            }
        }

    }

    static algoDemiPlan(points) {
        let result = [];
        let n = points.length;
        let previous, current, k, f, mc;

        for (let i = 0; i < n -1; i++) {
            for (let j = i + 1; j < n; j++) {
                mc = true;
                f = this.findFirst(i, j, points);
                previous = Coord2D.tour(points[i], points[j], points[f]);
                k = f + 1;

                while (k < n && mc) {
                    if (k != i && k != j) {
                        current = Coord2D.tour(points[i], points[j], points[k]);
                
                        if (current == 0) {
                            throw new Error("alignement", [points[i], points[j], points[k]]);
                        } else if (current != previous) {
                            mc = false;
                        }
                
                        previous = current;
                        
                    }
                    
                    k++;
                    
                }
                

           
                if (k == n && previous == current) {
                    if (current > 0) {
                        result.push([points[i], points[j]]);
                    } else if (current < 0) {
                        result.push([points[j], points[i]]);
                    } else if (current == 0) {
                        throw new Error("alignement", [points[i], points[j], points[k]]);
                    }
                }
                
            }
        }
      

        return result;
    }

    static findNext(currentPoint, points) {
        let nextPoint = points[0];
        if (currentPoint == nextPoint){
            nextPoint = points[1]; 
        }

        for (let i = 1; i < points.length; i++) {
          
            if (Coord2D.tour(currentPoint, nextPoint, points[i]) === -1) {
                nextPoint = points[i];
            }
        }
        
        return nextPoint;
    }
    
   

    static minY(points){
        let min = points[0]; 
        for (let i = 0; i < points.length; i++){
            if(points[i].y  < min.y){
                min = points[i]; 
            }
        }

        return min; 
    }


    static algoJarvis(points) {
        let min = this.minY(points);
        let result = [];
        result.push(min);
        let current;
        let previous = min;
        let iterations = 0; 
    
        while (current !== min && iterations < points.length) {
            current = this.findNext(previous, points);
            result.push(current);
            previous = current;
            iterations++;
        }
    
    
        return result;
    }
    
    algoGraham() {
    //todo: implementation de cet algo
    }
}
