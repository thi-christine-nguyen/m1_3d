#!/bin/sh
bindir=$(pwd)
cd /home/e20170004425/Bureau/M1_Imagine/Modelisation/TP8/src/
export 

if test "x$1" = "x--debugger"; then
	shift
	if test "xYES" = "xYES"; then
		echo "r  " > $bindir/gdbscript
		echo "bt" >> $bindir/gdbscript
		/usr/bin/gdb -batch -command=$bindir/gdbscript --return-child-result /home/e20170004425/Bureau/M1_Imagine/Modelisation/TP8/build/gmap 
	else
		"/home/e20170004425/Bureau/M1_Imagine/Modelisation/TP8/build/gmap"  
	fi
else
	"/home/e20170004425/Bureau/M1_Imagine/Modelisation/TP8/build/gmap"  
fi
